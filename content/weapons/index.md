---
title: Weapons
categories:
  - Battlefield 1942
layout: layouts/details.njk
grid: "halves thirds"
origin: https://web.archive.org/web/20030628024228/http://www.galactic-conquest.net/index.php?page=&action=show&id=2849
details:
  - name: Handheld
    icon: sw/han-solo-blaster.svg
    items:
      - name: Vibroblade
        image: img/game/weapons/handheld/alliesknife.png
      - name: E-11 Blaster Rifle
        image: img/game/weapons/handheld/e-11/main.png
        description: "The BlasTech E-11 Blaster Rifle is the standard sidearm for Imperial stormtroopers as it offers the great range and heavy damage of a traditional long barrelled rifle in a compact, easy-to-wield weapon. The E-11 has a light well-balanced design that permits accurate one-handed fire. This rifle has good long-range targeting because of the extendable sighting stock, the barrel's advanced galven circuitry, and the computer-enhanced scope, which filters out smoke and haze and enhances vision in low-light conditions."
        link: ../galleries/weapons/#e-11-blaster-rifle
      - name: A-280 Blaster Rifle
        image: img/game/weapons/handheld/a-280/render.jpg
        description: "The BlasTech A280 Blaster Rifle was favored by marksmen and snipers for its accuracy and range. It was employed by those who worked in hostile environments, and was standard issue to the Alliance forces stationed at Echo Base on Hoth."
        link: ../galleries/weapons/#a-295-blaster-rifle
      - name: A-295 Blaster Rifle
        image: img/game/weapons/handheld/a-295/main.png
      - name: T-21 Light Repeating Blaster
        image: img/game/weapons/handheld/t-21/main.png
        description: "Significantly more powerful than the standard blaster rifle, the BlasTech T-21 Light Repeating Blaster is the most deadly Imperial blaster that can be transported and fired by one soldier. The T-21 proved a common support weapon for army and stormtrooper squads. Imperial artillery sections normally include at least one solder with a T-21 who will provide cover for other crewmen while they are setting up artillery pieces and heavy repeating blasters."
        link: ../galleries/weapons/#t-21-light-repeating-blaster
      - name: Tracker 16 Light Repeating Blaster
        image: img/game/weapons/handheld/tracker-16/main.png
        link: ../galleries/weapons/#tracker-16-light-repeating-blaster
      - name: E-11 Sniper Rifle
        image: img/game/weapons/handheld/e-11-sniper-rifle/main.png
      - name: Dressellian Projectile Rifle
        image: img/game/weapons/handheld/dresselian-projectile-rifle/main.png
        link: ../galleries/weapons/#dressellian-projectile-rifle
      - name: Disruptor
        image:
          - img/game/weapons/handheld/disruptor/main.png
          - img/game/weapons/handheld/disruptor/alternate.png
        link: ../galleries/weapons/#disruptor
      - name: Speeder Pistol
        image: img/game/weapons/handheld/speeder-pistol/main.png
        link: ../galleries/weapons/#speeder-pistol
      - name: DL-44 Heavy Blaster Pistol
        image: img/game/weapons/handheld/dl-44/main.png
        description: "The BlasTech DL-44 Heavy Blaster Pistol puts the power of a blaster rifle into a weapon not more than the size of a standard pistol. The DL-44 is designed for close-quarter combat, sacrificing range for power. Although it has a maximum range of 50 meters, the pistol is known to penetrate stormtrooper armor with ease."
        link: ../galleries/weapons/#dl-44-heavy-blaster-pistol
      - name: DL-18 Blaster Pistol
        description: "The BlasTech DL-18 Blaster Pistol is easily considered the most widely used blaster pistol in the galaxy. It is popular with many organizations, such as urban police forces, merchant traders and even the Rebel Alliance/New Republic. The DL-18 is sleek and lighter than other blaster pistols of its class but dishes out a respectable amount of power. The power pack is loaded into place in front of the trigger and has enough power for 100 shots. The DL-18 also has a built-in housing on top for a sight."
      - name: DH-17 Blaster Pistol
        image: img/game/weapons/handheld/dh-17/main.png
        description: "Designed for short-range combat with a maximum range of 120 meters, the BlasTech DH-17 Blaster Pistol is known to penetrate stormtrooper armor and low-level forcefields but not the hulls of starships, making it a weapon of choice for shipboard troopers. The DH-17 is an unremarkable weapon in appearance. However, it has one major grace: it is nearly identical to the E-11 blaster rifle. The controls and maintenance drills are identical between the two weapons, and many of the parts, including the trigger group and action, are interchangeable."
        link: ../galleries/weapons/#dh-17-blaster-pistol
  - name: Utilities
    icon: glyphs-poly/tools.svg
    items:
      - name: Lethal Injection
        image: img/game/weapons/utility/lethal-injection.png
      - name: Flechette Launcher
        image: img/game/weapons/utility/flechette/main.png
        link: ../galleries/weapons/#flechette-launcher
      - name: Merr-Sonn PLX-2M Missile System
        image: img/game/weapons/utility/plx2m.png
        description: "The Merr-Sonn PLX-2M Missile System is a shoulder-mounted personal anti-vehicular missile launcher. Unlike real-life Anti-Tank or Anti-Air man-portable systems, this one is not specialized for air or ground vehicles, but instead, is designed to be simultaneously effective against both."
      - name: Merr Sonn C-22 Frag Grenade
        image: img/game/weapons/utility/c-22/main.png
        description: "Standard issue as secondary weapons for Rebel and Imperial ground troops, grenades are particularly effective for use in cluttered terrain, where blasters are of limited use."
      - name: Concussion Grenade
        image: img/game/weapons/utility/concgrenrebels.png
      - name: Gas Grenade
        image:
          - img/game/weapons/utility/gas-grenade/alliance-gas-grenade.png
          - img/game/weapons/utility/gas-grenade/imperial-gas-grenade.png
        link: ../galleries/weapons/#gas-grenade
      - name: Detonation Kit
        image: img/game/weapons/utility/detonation-kit/main.png
        link: ../galleries/weapons/#detonation-kit
      - name: Mines
        image:
          - img/game/weapons/utility/landmine/impmine.png
          - img/game/weapons/utility/landmine/rebmine.png
          - img/game/weapons/utility/landmine/landmine.png
        link: ../galleries/weapons/#mines
      - name: Electrobinoculars
        image: img/game/weapons/utility/electrobinoculars/main.png
        description: "Electrobinoculars are handheld imaging devices used to view objects from a considerable distance. The sophisticated device displays images with an overlay of computerized information detailing the object's distance, relative and true azimuths, and elevation. The imaging apparatus functions in low light conditions, enhancing the view to readable levels."
        link: ../galleries/weapons/#electrobinoculars
      - name: Medical Pack
        image: img/game/weapons/utility/medpack.png
        description: "The Med Pack is the medics best weapon for fixing minor battlefield cuts and bruises, complete with bacta coated bandages and various other medical essentials."
      - name: Camouflage
        image: img/game/weapons/utility/cammo.png
      - name: Shield
        image: img/game/weapons/utility/shield.png
      - name: Hydrospanner
        image: img/game/weapons/utility/hydrospanner.png
  - name: Stationary
    icon: glyphs-poly/toolbox.svg
    items:
      - name: Emplacement Weapon, Heavy Blaster
        image: img/game/weapons/utility/eweb.png
        description: "This weapon has proved highly effective against vehicles and infantry units alike. The Heavy Blaster has on optimum range of two hundred meters with a maximum range of half a kilometer and offers enough firepower to punch through a snowspeeder's armour plating."
      - name: Anti-Personell Turret
        image: img/game/stationary/apturret.png
        description: "Lining the perimeter of Echo Base on Hoth were a series of cylindrical gun towers painted in the same stark white as the surrounding landscape. Manned by the brave Rebel troopers of the base, these cannons provided defensive fire during the Imperial assault on Hoth. The cannons, though powerful, had little effect on the heavily-armored AT-AT walkers."
      - name: Anti-Vehicle Turret
        image: img/game/stationary/anti-vehicle-turret/main.png
        description: "During the Battle of Hoth, the Rebel Alliance used its meager collection of laser artillery to delay the advancing Imperial forces. Mobile laser units such as these dish-shaped cannons delivered powerful blasts that were ultimately ineffective against the thick armor of the Imperial AT-AT walkers."
        link: ../galleries/weapons/#anti-vehicle-turret
---
