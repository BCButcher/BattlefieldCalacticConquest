---
title: Galactic Conquest Audio
categories:
  - Galactic Conquest
type: audio
layout: layouts/audio.njk
sections:
  - name: Original audio files
    description: "Music from the mod."
    items:
      - name: Briefing
        url: "/audio/briefing.mp3"
      - name: Menu
        url: "/audio/menu.mp3"
      - name: Original Theme
        url: "/audio/original_theme.mp3"
        description: "From release [0.1a](/about/#release-0-1)."
      - name: Hoth
        url: "/audio/hoth.mp3"
      - name: Tatooine
        url: "/audio/tatooine.mp3"
      - name: Deathstar
        url: "/audio/deathstar.mp3"
      - name: Judicator
        url: "/audio/judicator.mp3"
      - name: Tanaab
        url: "/audio/tanaab.mp3"
      - name: Bonus Race
        url: "/audio/bonusrace.mp3"
---
