---
title: Videos from 2021
categories:
  - Battlefield 1942
type: exhibit
layout: layouts/exhibit.njk
items:
  - name: "PHOENIX CELL"
    image: img/game/video/youtube-qDGer2Cgsmg.webp
    link: "https://www.youtube.com/watch?v=qDGer2Cgsmg"
    publisher: "P0SlTR0N"
    published: "2021-12-01"
  - name: "GALACTIC CONQUEST - BACK TO BLACK - EVENT NOV 6th , 2pm est"
    image: img/game/video/youtube-Ne-MXu8UoMY.webp
    link: "https://www.youtube.com/watch?v=Ne-MXu8UoMY"
    publisher: "P0SlTR0N"
    published: "2021-10-30"
  - name: "Galactic Conquest -  May the 4th CTF Extravaganza.mkv"
    image: img/game/video/youtube--F_mQ0BLPdI.webp
    link: "https://www.youtube.com/watch?v=-F_mQ0BLPdI"
    publisher: "P0SlTR0N"
    published: "2021-04-26"
---
