---
title: Footage Gallery
categories:
  - Battlefield 1942
type: gallery
layout: layouts/gallery.njk
sections:
  - name: 2025
    items:
      - name: Rebel Uprising Galactic Conquest Event
        link: "./2025-01-11-rebel-uprising-galactic-conquest-event"
        description: "2025-01-11"
        origin: https://web.archive.org/web/20250111144130/https://forum.helloclan.eu/threads/10556/
        image: img/community/events/2025-01-11.png
  - name: 2024
    items:
      - name: Imperial Counteroffensive Galactic Conquest Event
        link: "./2024-11-30-imperial-counteroffensive-galactic-conquest-event/"
        description: "2024-11-30"
        origin: https://web.archive.org/web/20241201094114/https://forum.helloclan.eu/threads/10529/
        image: img/community/events/2024-11-30.jpg
      - name: Rapscallion's Heart Galactic Conquest Event
        description: "2024-08-17"
        origin: https://web.archive.org/web/20240817194156/https://forum.helloclan.eu/threads/10489/
        image: img/community/events/2024-08-17.png
      - name: Star Destroyer Event
        link: "./2024-06-22-star-destroyer-event/"
        description: "2024-06-22"
        origin: https://web.archive.org/web/20240723105219/https://forum.helloclan.eu/threads/10446/
        image: img/community/events/2024-06-22.jpg
      - name: May The Fourth Be With You Event
        link: "./2024-05-04-may-the-fourth-be-with-you-event/"
        description: "2024-05-04"
        origin: https://web.archive.org/web/20240723105005/https://forum.helloclan.eu/threads/10407/
        image: img/community/events/2024-05-04.png
      - name: Imperial Assault Event
        link: "./2024-03-23-imperial-assault-event/"
        description: "2024-03-23"
        origin: https://web.archive.org/web/20240723104726/https://forum.helloclan.eu/threads/10374/
        image: img/community/events/2024-03-23.png
      - name: Ancillary Campaign II Event
        link: "./2024-01-13-ancillary-campaign-ii-event/"
        description: "2024-01-13"
        origin: https://web.archive.org/web/20240723104519/https://forum.helloclan.eu/threads/10328/
        image: img/community/events/2024-01-13.png
  - name: 2023
    items:
      - name: Ancillary Campaign Event
        link: "./2023-11-18-ancillary-campaign-event/"
        description: "2023-11-18"
        origin: https://web.archive.org/web/20240723104320/https://forum.helloclan.eu/threads/10278/
        image: img/community/events/2023-11-18.jpg
      - name: Hell or High Water Event
        link: "./2023-08-19-hell-or-high-water-event/"
        description: "2023-08-19"
        image: img/community/events/2023-08-19.jpg
      - name: Revenge Of The Sixth
        link: "./2023-05-06-revenge-of-the-sixth/"
        description: "2023-05-06"
        image: img/community/events/2023-05-06.jpg
      - name: Rejuve Event
        link: "./2023-04-01-rejuve-event/"
        description: "2023-04-01"
        image: img/community/events/2023-04-01.jpg
      - name: Full Speed Ahead Event
        link: "./2023-02-25-full-speed-ahead-event/"
        description: "2023-02-25"
        image: img/community/events/2023-02-25.png
  - name: 2022
    items:
      - name: Galactic Conquest 8.5 Release Event
        link: "./2022-11-26-galactic-conquest-85-release-event/"
        description: "2022-11-26"
        image: img/community/events/2022-11-26.png
      - name: Other
        link: "./2022/"
        description: "Video-archive from 2022."
        image: img/game/video/youtube-hLCOWRIVA4U.webp
  - name: Older
    items:
      - name: 2021
        link: "./2021/"
        description: Video-archive from 2021.
        image: img/game/video/youtube-qDGer2Cgsmg.webp
      - name: 2019
        link: "./2019/"
        description: Video-archive from 2019.
        image: img/game/video/youtube-j9MlUSc1Lzo.webp
      - name: "Galactic Conquest Scenes"
        image: img/game/video/youtube-i_E2wH1_b9k.webp
        link: "https://www.youtube.com/watch?v=i_E2wH1_b9k"
        publisher: "LuccaWulf"
        published: "2018-09-16"
      - name: "PUBLIC BETA GC 8.0 - Test Footage (7)"
        image: img/game/video/youtube-0Qiv-U3J_uA.jpg
        link: "https://www.youtube.com/watch?v=0Qiv-U3J_uA"
        publisher: "P0SlTR0N"
        published: "2016-06-21"
---
