---
title: Rejuve Event
description: "2023-04-01"
categories:
  - Battlefield 1942
image: img/community/events/2023-04-01.jpg
type: exhibit
layout: layouts/exhibit.njk
items:
  - name: "Battlefield 1942 / Galactic Conquest Mod: Experience Star Wars Like Never Before"
    image: img/game/video/youtube-wpqydVWQ7AI.webp
    link: "https://www.youtube.com/watch?v=wpqydVWQ7AI"
    publisher: "Winbean winlite"
    published: "2023-04-03"
  - name: "Battlefield 1942 Transformed: Galactic Conquest Mod Takes You to a Galaxy Far, Far Away!"
    image: img/game/video/youtube-NLmjhrHERh4.webp
    link: "https://www.youtube.com/watch?v=NLmjhrHERh4"
    publisher: "Winbean winlite"
    published: "2023-04-02"
  - name: "Battlefield 1942/ Galactic Conquest Mod Takes You to a Galaxy Far, Far Away! Sunday, 4/1/2023"
    image: img/game/video/youtube-wCEfRdwYfXY.webp
    link: "https://www.youtube.com/watch?v=wCEfRdwYfXY"
    publisher: "Winbean winlite"
    published: "2023-04-02"
---
