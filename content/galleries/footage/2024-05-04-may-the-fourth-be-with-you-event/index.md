---
title: May The Fourth Be With You Event
description: "2024-05-04"
categories:
  - Battlefield 1942
origin: https://web.archive.org/web/20240723105005/https://forum.helloclan.eu/threads/10407/
image: img/community/events/2024-05-04.png
type: exhibit
layout: layouts/exhibit.njk
items:
  - name: "BF1942/Galactic Conquests GC: Bespin - A Battle in the City in the Clouds"
    image: img/game/video/rumble-v4qzcnh.jpg
    link: "https://rumble.com/v4th73h-bf1942galactic-conquests-gc-bespin-a-battle-in-the-city-in-the-clouds.html"
    publisher: "Winbean45"
    published: "2024-05-05"
  - name: "Battlefield 1942/Galactic Conquests: GC Hoth- Battle in the Frozen Mountain Fortress"
    image: img/game/video/rumble-v4qzfq3.jpg
    link: "https://rumble.com/v4tha63-battlefield-1942galactic-conquests-gc-hoth-battle-in-the-frozen-mountain-fo.html"
    publisher: "Winbean45"
    published: "2024-05-05"
  - name: "Battlefield 1942/Galactic Conquest: GCV Mini Wasteland - no armor support."
    image: img/game/video/rumble-v4qzcnh.jpg
    link: "https://rumble.com/v4tmg80-battlefield-1942galactic-conquest-gcv-mini-wasteland-no-armor-support..html"
    publisher: "Winbean45"
    published: "2024-05-06"
  - name: "Battlefield 1942/Galactic Conquest: Endor's Verdant Fury"
    image: img/game/video/rumble-v4r4hv8.jpg
    link: "https://rumble.com/v4tmc95-battlefield-1942galactic-conquest-endors-verdant-fury.html"
    publisher: "Winbean45"
    published: "2024-05-06"
  - name: "Battlefield 1942/Galactic Conquest: Ryloth Desperate Stand"
    image: img/game/video/rumble-v4r4kbi.jpg
    link: "https://rumble.com/v4tmepf-battlefield-1942galactic-conquest-ryloth-desperate-stand.html"
    publisher: "Winbean45"
    published: "2024-05-06"
  - name: "Battlefield 1942/Galactic Conquest: Operation Mini Wayfar"
    image: img/game/video/rumble-v4ra8pn.jpg
    link: "https://rumble.com/v4ts31w-battlefield-1942galactic-conquest-operation-mini-wayfar.html"
    publisher: "Winbean45"
    published: "2024-05-07"
  - name: "Battlefield 1942/Galactic Conquest: Mini Dant Skirmish"
    image: img/game/video/rumble-v4rabfc.jpg
    link: "https://rumble.com/v4ts5rl-battlefield-1942galactic-conquest-mini-dant-skirmish.html"
    publisher: "Winbean45"
    published: "2024-05-07"
---
