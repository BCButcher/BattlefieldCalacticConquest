---
title: Revenge Of The Sixth
description: "2023-05-06"
categories:
  - Battlefield 1942
image: img/community/events/2023-05-06.jpg
type: exhibit
layout: layouts/exhibit.njk
items:
  - name: "Battlefield 1942 / Galactic Conquests more than less a friendly warm-up round."
    image: img/game/video/youtube-Mr8L41so6vI.webp
    link: "https://www.youtube.com/watch?v=Mr8L41so6vI"
    publisher: "Winbean winlite"
    published: "2023-05-07"
  - name: "Capture the Flag Like Never Before in Galactic Conquest Mod for Battlefield 1942."
    image: img/game/video/youtube-N96dTWGws6Q.webp
    link: "https://www.youtube.com/watch?v=N96dTWGws6Q"
    publisher: "Winbean winlite"
    published: "2023-05-07"
  - name: "Conquer the Skies of Bespin in Galactic Conquest Mod for Battlefield 1942!"
    image: img/game/video/youtube-8zwo-cjLNwE.webp
    link: "https://www.youtube.com/watch?v=8zwo-cjLNwE"
    publisher: "Winbean winlite"
    published: "2023-05-07"
  - name: "Experience Intense Urban Fighting in Galactic Conquest Mod for Battlefield 1942"
    image: img/game/video/youtube-32stJclvs7A.webp
    link: "https://www.youtube.com/watch?v=32stJclvs7A"
    publisher: "Winbean winlite"
    published: "2023-05-07"
  - name: "Exploring the Dark Caves of Galactic Conquest Mod in Battlefield 1942!"
    image: img/game/video/youtube-inlEgugoOvI.webp
    link: "https://www.youtube.com/watch?v=inlEgugoOvI"
    publisher: "Winbean winlite"
    published: "2023-05-07"
  - name: "Relaxing Space Battles  Galactic Conquest for Battlefield 1942 Explore the Galaxy at Your Own Pace!"
    image: img/game/video/youtube-d9mCUNOS0TI.webp
    link: "https://www.youtube.com/watch?v=d9mCUNOS0TI"
    publisher: "Winbean winlite"
    published: "2023-05-07"
  - name: "Unleash the Power of Armor in Galactic Conquest Mod for Battlefield 1942 Vehicles and Tactics Await!"
    image: img/game/video/youtube-nfKJA9WnSI4.webp
    link: "https://www.youtube.com/watch?v=nfKJA9WnSI4"
    publisher: "Winbean winlite"
    published: "2023-05-07"
---
