---
title: Rebel Uprising Galactic Conquest Event
description: "2025-01-11"
categories:
  - Battlefield 1942
origin: https://web.archive.org/web/20250111144130/https://forum.helloclan.eu/threads/10556/
image: img/community/events/2025-01-11.png
type: exhibit
layout: layouts/exhibit.njk
items:
  - name: "Battlefield 1942 / Galactic Conquest : Livestream - 01/11/2025"
    image: img/game/video/rumble-v663cuy.jpg
    link: "https://rumble.com/v68brvs-battlefield-1942-galactic-conquest-livestream-01112025.html"
    publisher: "Winbean45"
    published: "2025-01-11"
---
