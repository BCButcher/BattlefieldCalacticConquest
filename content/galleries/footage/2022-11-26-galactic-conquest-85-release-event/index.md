---
title: Galactic Conquest 8.5 Release Event
description: "2022-11-26"
categories:
  - Battlefield 1942
image: img/community/events/2022-11-26.png
type: exhibit
layout: layouts/exhibit.njk
items:
  - name: "Battlefield 1942, Galactic Conquest MOD, Saturday, November 26, 2022 map GC HOTH"
    image: img/game/video/rumble-v1uv99a.jpg
    link: "https://rumble.com/v1xhems-battlefield-1942-galactic-conquest-mod-saturday-november-26-2022-map-gc-hot.html"
    publisher: Winbean45
    published: "2022-11-28"
  - name: "Battlefield 1942, Galactic Conquest MOD, Saturday, November 26, 2022 map GC JUDICATOR PUSH"
    image: img/game/video/rumble-v1uwrv4.jpg
    link: "https://rumble.com/v1xix8c-battlefield-1942-galactic-conquest-mod-saturday-november-26-2022-map-gc-jud.html"
    publisher: Winbean45
    published: "2022-11-28"
  - name: "Battlefield 1942, Galactic Conquest MOD, Saturday, November 26, 2022 map GC MINI WASTELAND"
    image: img/game/video/rumble-v1uwjoo.jpg
    link: "https://rumble.com/v1xip1w-battlefield-1942-galactic-conquest-mod-saturday-november-26-2022-map-gc-hot.html"
    publisher: Winbean45
    published: "2022-11-28"
  - name: "Battlefield 1942, Galactic Conquest MOD, Saturday, November 26, 2022 map GC PODRACE."
    image: img/game/video/rumble-v1uxa2y.jpg
    link: "https://rumble.com/v1xjffw-battlefield-1942-galactic-conquest-mod-saturday-november-26-2022-map-gc-pod.html"
    publisher: Winbean45
    published: "2022-11-28"
  - name: "Battlefield 1942, Galactic Conquest MOD, Saturday, November 26, 2022 map GC TASKFORCE"
    image: img/game/video/rumble-v1uxrpy.jpg
    link: "https://rumble.com/v1xjx2m-battlefield-1942-galactic-conquest-mod-saturday-november-26-2022-map-gc-tas.html"
    publisher: Winbean45
    published: "2022-11-28"
  - name: "Battlefield 1942, Galactic Conquest MOD, Saturday, November 26, 2022 map GC TATOOINE"
    image: img/game/video/rumble-v1uxhiq.jpg
    link: "https://rumble.com/v1xjmve-battlefield-1942-galactic-conquest-mod-saturday-november-26-2022-map-gctato.html"
    publisher: Winbean45
    published: "2022-11-28"
  - name: "Battlefield 1942, Galactic Conquest MOD, Saturday, November 26, 2022 map  GC RYLOTH"
    image: img/game/video/rumble-v1uu3uk.jpg
    link: "https://rumble.com/v1xg98c-battlefield-1942-galactic-conquest-mod-saturday-november-26-2022-map-gc-ryl.html"
    publisher: Winbean45
    published: "2022-11-28"
  - name: "Battlefield 1942, Galactic Conquest MOD Event, Saturday, November 26, 2022 map GC MINI CAVES"
    image: img/game/video/rumble-v1urzqk.jpg
    link: "https://rumble.com/v1xe54m-battlefield-1942-galactic-conquest-mod-event-saturday-november-26-2022-map-.html"
    publisher: Winbean45
    published: "2022-11-27"
  - name: "Battlefield 1942, Galactic Conquest MOD, Saturday, November 26, 2022 map GC ALARIS PRIME"
    image: img/game/video/rumble-v1uufmq.jpg
    link: "https://rumble.com/v1xgl0i-battlefield-1942-galactic-conquest-mod-saturday-november-26-2022-map-gc-ala.html"
    publisher: Winbean45
    published: "2022-11-27"
  - name: "Battlefield 1942, Galactic Conquest MOD, Saturday, November 26, 2022 map GC ANCHORHEAD"
    image: img/game/video/rumble-v1utbnu.jpg
    link: "https://rumble.com/v1xfh1w-battlefield-1942-galactic-conquest-mod-saturday-november-26-2022-map-gc-anc.html"
    publisher: Winbean45
    published: "2022-11-27"
  - name: "Battlefield 1942, Galactic Conquest MOD, Saturday, November 26, 2022 map GC BESPIN"
    image: img/game/video/rumble-v1utmx4.jpg
    link: "https://rumble.com/v1xfsb6-battlefield-1942-galactic-conquest-mod-saturday-november-26-2022-map-gc-bes.html"
    publisher: Winbean45
    published: "2022-11-27"
---
