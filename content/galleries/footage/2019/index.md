---
title: Videos from 2019
categories:
  - Battlefield 1942
type: exhibit
layout: layouts/exhibit.njk
items:
  - name: "Alaris Prime at a glance"
    image: img/game/video/youtube-j9MlUSc1Lzo.webp
    link: "https://www.youtube.com/watch?v=j9MlUSc1Lzo"
    publisher: "P0SlTR0N"
    published: "2019-08-03"
  - name: "Galactic Conquest - How to  (Boarding the AT AT)"
    image: img/game/video/youtube-Rw_lUO3DLF0.webp
    link: "https://www.youtube.com/watch?v=Rw_lUO3DLF0"
    publisher: "P0SlTR0N"
    published: "2019-08-08"
  - name: "Galactic Conquest 8.0 - Primer Event"
    image: img/game/video/youtube-a13w9U9_IwQ.jpg
    link: "https://www.youtube.com/watch?v=a13w9U9_IwQ"
    publisher: "P0SlTR0N"
    published: "2019-07-31"
  - name: "Galactic Conquest 8.2"
    image: img/game/video/youtube-5MdSKJ-4JSo.webp
    link: "https://www.youtube.com/watch?v=5MdSKJ-4JSo"
    publisher: "P0SlTR0N"
    published: "2019-08-15"
  - name: "Galactic Conquest Mod 8.0  - Multiplayer Highlights (1)"
    image: img/game/video/youtube-2swXGAekKBI.webp
    link: "https://www.youtube.com/watch?v=2swXGAekKBI"
    publisher: "P0SlTR0N"
    published: "2019-07-28"
  - name: "Hoth Highlights"
    image: img/game/video/youtube-xr4Xz8an3Bg.webp
    link: "https://www.youtube.com/watch?v=xr4Xz8an3Bg"
    publisher: "P0SlTR0N"
    published: "2019-08-06"
  - name: "Mini Hoth"
    image: img/game/video/youtube-LlV8zFYihkA.webp
    link: "https://www.youtube.com/watch?v=LlV8zFYihkA"
    publisher: "P0SlTR0N"
    published: "2019-07-20"
  - name: "PUBLIC BETA GC 8.0 - Player Perspective"
    image: img/game/video/youtube-Cs3WnvTkR7E.jpg
    link: "https://www.youtube.com/watch?v=Cs3WnvTkR7E"
    publisher: "P0SlTR0N"
    published: "2019-08-06"
  - name: "PUBLIC BETA GC 8.0 - Test Footage (5)"
    image: img/game/video/youtube-KZ8TqUcSBbY.jpg
    link: "https://www.youtube.com/watch?v=KZ8TqUcSBbY"
    publisher: "P0SlTR0N"
    published: "2019-07-06"
  - name: "PUBLIC BETA GC 8.0 - Test Footage (9)"
    image: img/game/video/youtube-cwkgiDkiVC4.jpg
    link: "https://www.youtube.com/watch?v=cwkgiDkiVC4"
    publisher: "P0SlTR0N"
    published: "2019-08-25"
---
