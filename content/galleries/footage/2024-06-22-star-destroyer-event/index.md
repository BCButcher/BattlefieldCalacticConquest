---
title: Star Destroyer Event
description: "2024-06-22"
categories:
  - Battlefield 1942
origin: https://web.archive.org/web/20240723105219/https://forum.helloclan.eu/threads/10446/
image: img/community/events/2024-22-06.jpg
type: exhibit
layout: layouts/exhibit.njk
items:
  - name: "BF1942⧸GC： Secrets Unveiled - Rebel Success on GC Kryos"
    image: img/game/video/rumble-v51c34o.jpg
    link: "https://rumble.com/v53obv0-bf1942gc-secrets-unveiled-rebel-success-on-gc-kryos.html"
    publisher: "Winbean45"
    published: "2024-06-25"
  - name: "BF1942⧸GC： Imperial Defense Prevails on GC Tanaab"
    image: img/game/video/rumble-v51c0v2.jpg
    link: "https://rumble.com/v53o9le-bf1942gc-imperial-defense-prevails-on-gc-tanaab.html"
    publisher: "Winbean45"
    published: "2024-06-26"
  - name: "BF1942⧸GC： Mostly joy riding with TheoNeo＜CP＞ on GC BCANYON"
    image: img/game/video/rumble-v51bx30.jpg
    link: "https://rumble.com/v53o5tr-bf1942gc-mostly-joy-riding-with-theoneocp-on-gc-bcanyon.html"
    publisher: "Winbean45"
    published: "2024-06-25"
  - name: "BF1942⧸GC： Siege of Ryloth - Imperial Resurgence"
    image: img/game/video/rumble-v517zlm.jpg
    link: "https://rumble.com/v53k8cd-bf1942gc-siege-of-ryloth-imperial-resurgence.html"
    publisher: "Winbean45"
    published: "2024-06-25"
  - name: "BF1942⧸GC： Rebel Raid on the Judicator - Ship Breached"
    image: img/game/video/rumble-v5181a1.jpg
    link: "https://rumble.com/v53ka0s-bf1942gc-rebel-raid-on-the-judicator-ship-breached.html"
    publisher: "Winbean45"
    published: "2024-06-25"
  - name: "BF1942⧸GC： It's Raining Escape Pods on GC BESPIN NIGHT"
    image: img/game/video/rumble-v514orz.jpg
    link: "https://rumble.com/v53gxiq-bf1942gc-its-raining-escape-pods-on-gc-bespin-night.html"
    publisher: "Winbean45"
    published: "2024-06-25"
  - name: "BF1942⧸GC： Rebel Defeat on GC Mini Wayfar"
    image: img/game/video/rumble-v514otf.jpg
    link: "https://rumble.com/v53gxk6-bf1942gc-rebel-defeat-on-gc-mini-wayfar.html"
    publisher: "Winbean45"
    published: "2024-06-24"
  - name: "BF1942⧸GC： Galactic Blitz - The Imperial Skiff Encounter"
    image: img/game/video/rumble-v516uig.jpg
    link: "https://rumble.com/v53j397-bf1942gc-galactic-blitz-the-imperial-skiff-encounter.html"
    publisher: "Winbean45"
    published: "2024-06-26"
  - name: "Battlefield Galactic Conquest Taskforce Multiplayer Gameplay"
    image: img/game/video/youtube-8OgWpVbI560.webp
    link: "https://www.youtube.com/watch?v=8OgWpVbI560"
    publisher: "SuperFjordmannen"
    published: "2024-06-20"
---
