---
title: Full Speed Ahead Event
description: "2023-02-25"
categories:
  - Battlefield 1942
image: img/community/events/2023-02-25.png
type: exhibit
layout: layouts/exhibit.njk
items:
  - name: "Battlefield 1942, Galactic Conquest MOD add graphical mod February 25, 2023 round 1"
    image: img/game/video/youtube-lC1ftV0l_5Q.webp
    link: "https://www.youtube.com/watch?v=lC1ftV0l_5Q"
    publisher: "Winbean winlite"
    published: "2023-02-26"
---
