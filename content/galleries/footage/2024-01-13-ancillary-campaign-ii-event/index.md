---
title: Ancillary Campaign II Event
description: "2024-01-13"
categories:
  - Battlefield 1942
origin: https://web.archive.org/web/20240723104519/https://forum.helloclan.eu/threads/10328/
image: img/community/events/2024-01-13.png
type: exhibit
layout: layouts/exhibit.njk
items:
  - name: "Galactic Conquest： Hoth Showdown - Death Squadron vs. Rebel Evacuation ⧸ Battlefield 1942"
    image: img/game/video/rumble-v44wf0r.jpg
    link: "https://rumble.com/v47hqoo-galactic-conquest-hoth-showdown-death-squadron-vs.-rebel-evacuation-battlef.html"
    publisher: "Winbean45"
    published: "2024-01-17"
  - name: "Galactic Conquest： Battle for Bespin's Future - Rebels vs. Empire ⧸ Battlefield 1942"
    image: img/game/video/rumble-v44vvwc.jpg
    link: "https://rumble.com/v47h7ko-galactic-conquest-battle-for-bespins-future-rebels-vs.-empire-battlefield-1.html"
    publisher: "Winbean45"
    published: "2024-01-17"
  - name: "GC Judicator Push： Rebel Infiltration and Internal Struggle on the Imperial Ship ⧸ Battlefield 1942"
    image: img/game/video/rumble-v44zzxl.jpg
    link: "https://rumble.com/v47lbk9-gc-judicator-push-rebel-infiltration-and-internal-struggle-on-the-imperial-.html"
    publisher: "Winbean45"
    published: "2024-01-17"
  - name: "GC Ryloth： Alliance vs. Empire - The Battle for a Forsaken World ⧸ Battlefield 1942"
    image: img/game/video/rumble-v451h00.jpg
    link: "https://rumble.com/v47msmo-gc-ryloth-alliance-vs.-empire-the-battle-for-a-forsaken-world-battlefield-1.html"
    publisher: "Winbean45"
    published: "2024-01-18"
  - name: "Galactic Conquest： Endor's Last Stand - Rebel Assault on Death Star Shield"
    image: img/game/video/rumble-v457iof.jpg
    link: "https://rumble.com/v47sua9-galactic-conquest-endors-last-stand-rebel-assault-on-death-star-shield.html"
    publisher: "Winbean45"
    published: "2024-01-18"
  - name: "GC Mini Cave： Epic Power Struggle in the Caverns of Dagobah ⧸ Battlefield 1942"
    image: img/game/video/rumble-v44xpal.jpg
    link: "https://rumble.com/v47j0yi-gc-mini-cave-epic-power-struggle-in-the-caverns-of-dagobah-battlefield-1942.html"
    publisher: "Winbean45"
    published: "2024-01-17"
---
