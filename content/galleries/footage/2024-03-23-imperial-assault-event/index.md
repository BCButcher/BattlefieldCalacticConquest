---
title: Imperial Assault Event
description: "2024-03-23"
categories:
  - Battlefield 1942
origin: https://web.archive.org/web/20240723104726/https://forum.helloclan.eu/threads/10374/
image: img/community/events/2024-23-03.png
type: exhibit
layout: layouts/exhibit.njk
items:
  - name: "BF1942⧸GC： GC TATOOINE The desperate cry for help to all rebels!!"
    image: img/game/video/rumble-v4jdevq.jpg
    link: "https://rumble.com/v4lymtw-bf1942gc-gc-tatooine-the-desperate-cry-for-help-to-all-rebels.html"
    publisher: "Winbean45"
    published: "2024-03-27"
  - name: "Battlefield 1942⧸Galactic conquest： GC RYLOTH CTF!!"
    image: img/game/video/rumble-v4jc92b.jpg
    link: "https://rumble.com/v4lxh02-battlefield-1942galactic-conquest-gc-ryloth-ctf.html"
    publisher: "Winbean45"
    published: "2024-03-27"
  - name: "Battlefield 1942⧸Galactic conquest： GC MINI COVE!!!"
    image: img/game/video/rumble-v4jcb0k.jpg
    link: "https://rumble.com/v4lxiyb-battlefield-1942galactic-conquest-gc-mini-cove.html"
    publisher: "Winbean45"
    published: "2024-03-27"
  - name: "Battlefield 1942⧸Galactic： Galactic Wayfarer's Gambit： The Conflict Unleashed"
    image: img/game/video/rumble-v4j359c.jpg
    link: "https://rumble.com/v4lodbo-battlefield-1942galactic-galactic-wayfarers-gambit-the-conflict-unleashed.html"
    publisher: "Winbean45"
    published: "2024-03-26"
  - name: "Battlefield 1942⧸Galactic conquest： GC CORELLIA Conflict"
    image: img/game/video/rumble-v4j32je.jpg
    link: "https://rumble.com/v4loalq-battlefield-1942galactic-conquest-gc-corellia-conflict..html"
    publisher: "Winbean45"
    published: "2024-03-26"
  - name: "Battlefield 1942： fire fight on Bespin - Galactic Conquest TEST"
    image: img/game/video/rumble-v4iweao.jpg
    link: "https://rumble.com/v4lhmeo-battlefield-1942-fire-fight-on-bespin-galactic-conquest-test.html"
    publisher: "Winbean45"
    published: "2024-03-25"
  - name: "Battlefield 1942： Dusk on Bespin - Galactic Conquest"
    image: img/game/video/rumble-v4iwbif.jpg
    link: "https://rumble.com/v4lhjmf-battlefield-1942-dusk-on-bespin-galactic-conquest.html"
    publisher: "Winbean45"
    published: "2024-03-25"
  - name: "Battlefield 1942 ⧸ GALACTIC CONQUEST GC MINI CAVE Warm up!"
    image: img/game/video/rumble-v4ikv86.jpg
    link: "https://rumble.com/v4l63e9-battlefield-1942-galactic-conquest-gc-mini-cave-warm-up.html"
    publisher: "Winbean45"
    published: "2024-03-24"
  - name: "Battlefield 1942 ⧸ GALACTIC CONQUEST GC MINI DANT"
    image: img/game/video/rumble-v4inx85.jpg
    link: "https://rumble.com/v4l95de-battlefield-1942-galactic-conquest-gc-mini-dant.html"
    publisher: "Winbean45"
    published: "2024-03-24"
  - name: "Battlefield 1942 ⧸ GALACTIC CONQUEST GC MINI WASTELAND"
    image: img/game/video/rumble-v4inz4w.jpg
    link: "https://rumble.com/v4l97a5-battlefield-1942-galactic-conquest-gc-mini-wasteland.html"
    publisher: "Winbean45"
    published: "2024-03-24"
  - name: "Battlefield 1942 ⧸ GALACTIC CONQUEST. GC MINI WAYFAR Fireside Warm-Up"
    image: img/game/video/rumble-v4ikuch.jpg
    link: "https://rumble.com/v4l62ik-battlefield-1942-galactic-conquest.-gc-mini-wayfar-fireside-warm-up..html"
    publisher: "Winbean45"
    published: "2024-03-24"
  - name: "BF1942 Galactic Conquest. *NEW* BESPIN CTF mode EPIC"
    image: img/game/video/youtube-QnMm80_bAAQ.webp
    link: "https://www.youtube.com/watch?v=QnMm80_bAAQ"
    publisher: "0poIE"
    published: "2024-03-24"
---
