---
title: Hell or High Water Event
description: "2023-08-19"
categories:
  - Battlefield 1942
image: img/community/events/2023-08-19.jpg
type: exhibit
layout: layouts/exhibit.njk
items:
  - name: "Anchorhead Showdown: Galactic Conquest Unleashed"
    image: img/game/video/youtube-7lvjws34pyg.webp
    link: "https://www.youtube.com/watch?v=7lvjws34pyg"
    publisher: "Winbean winlite"
    published: "2023-08-21"
  - name: "Mini Chandrila Expedition: Galactic Conquest Unveiled"
    image: img/game/video/youtube-LPXtHmfdmYE.webp
    link: "https://www.youtube.com/watch?v=LPXtHmfdmYE"
    publisher: "Winbean winlite"
    published: "2023-08-21"
  - name: "Bespin Night: Galactic Conquest Under the Stars"
    image: img/game/video/youtube-opm00iN_RvA.webp
    link: "https://www.youtube.com/watch?v=opm00iN_RvA"
    publisher: "Winbean winlite"
    published: "2023-08-20"
  - name: "Conquering Ryloth: Galactic Conquest Adventure"
    image: img/game/video/youtube-9yXwEPbFIjM.webp
    link: "https://www.youtube.com/watch?v=9yXwEPbFIjM"
    publisher: "Winbean winlite"
    published: "2023-08-20"
  - name: "Mini Wasteland Adventures in Galactic Conquest"
    image: img/game/video/youtube-sWi5m4b2ohE.webp
    link: "https://www.youtube.com/watch?v=sWi5m4b2ohE"
    publisher: "Winbean winlite"
    published: "2023-08-20"
  - name: "Subterranean Skirmishes: Galactic Conquest in the Caves"
    image: img/game/video/youtube-0oj3QHpNdT0.webp
    link: "https://www.youtube.com/watch?v=0oj3QHpNdT0"
    publisher: "Winbean winlite"
    published: "2023-08-20"
---
