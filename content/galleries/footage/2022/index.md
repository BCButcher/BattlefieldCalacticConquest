---
title: Other videos from 2022
categories:
  - Battlefield 1942
type: exhibit
layout: layouts/exhibit.njk
items:
  - name: "Core Compressor"
    image: img/game/video/youtube-hLCOWRIVA4U.webp
    link: "https://www.youtube.com/watch?v=hLCOWRIVA4U"
    publisher: "P0SlTR0N"
    published: "2022-10-05"
  - name: "Galactic Conquest - #1 on Judicator Push"
    image: img/game/video/youtube-j7zl5eeq5us.webp
    link: "https://www.youtube.com/watch?v=j7zl5eeq5us"
    publisher: "LuccaWulf"
    published: "2022-08-15"
  - name: "Galactic Conquest - #1 on Bespin Night"
    image: img/game/video/youtube-OSIOviRyvjs.webp
    link: "https://www.youtube.com/watch?v=OSIOviRyvjs"
    publisher: "LuccaWulf"
    published: "2022-05-15"
  - name: "Battlefield 1942 star wars episode Iv event part 1 from January 22 2022"
    image: img/game/video/youtube-CDK__o_fBUo.webp
    link: "https://www.youtube.com/watch?v=CDK__o_fBUo"
    publisher: E3gaming
    published: "2022-01-23"
  - name: "Battlefield 1942  star wars episode Iv event part 2 from  January 22  2022"
    image: img/game/video/youtube-yP48_xJwyuo.webp
    link: "https://www.youtube.com/watch?v=yP48_xJwyuo"
    publisher: E3gaming
    published: "2022-01-24"
  - name: "Battlefield 1942 star wars episode Iv event part 3 from January 22 2022"
    image: img/game/video/youtube-Fe-EtsD4LW0.webp
    link: "https://www.youtube.com/watch?v=Fe-EtsD4LW0"
    publisher: E3gaming
    published: "2022-01-23"
---
