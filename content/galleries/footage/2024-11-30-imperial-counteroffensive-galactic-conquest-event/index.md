---
title: Imperial Counteroffensive Galactic Conquest Event
description: "2024-11-30"
categories:
  - Battlefield 1942
origin: https://web.archive.org/web/20241201094114/https://forum.helloclan.eu/threads/10529/
image: img/community/events/2024-11-30.jpg
type: exhibit
layout: layouts/exhibit.njk
items:
  - name: "Battlefield 1942 / Galactic Conquest : Livestream - 11/30/2024"
    image: img/game/video/rumble-v5sfgme.jpg
    link: "https://rumble.com/v5uoffb-battlefield-1942-galactic-conquest-livestream-11302024.html"
    publisher: "Winbean45"
    published: "2024-11-30"
  - name: "BF1942 Galactic Conquest"
    image: img/game/video/twitch-v2315117744.jpg
    link: "https://www.twitch.tv/videos/2315117744"
    publisher: "Omabroodje"
    published: "2024-11-30"
  - name: "BF1942 Galactic Conquest"
    image: img/game/video/twitch-v2315102150.jpg
    link: "https://www.twitch.tv/videos/2315102150"
    publisher: "Omabroodje"
    published: "2024-11-30"
---
