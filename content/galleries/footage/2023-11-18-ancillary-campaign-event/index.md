---
title: Ancillary Campaign Event
description: "2023-11-18"
categories:
  - Battlefield 1942
origin: https://web.archive.org/web/20240723104320/https://forum.helloclan.eu/threads/10278/
image: img/community/events/2023-11-18.jpg
type: exhibit
layout: layouts/exhibit.njk
items:
  - name: "Skyward Battlefields: Galactic Conquest - Ryloth"
    image: img/game/video/youtube-IyP10RVpmKE.webp
    link: "https://www.youtube.com/watch?v=IyP10RVpmKE"
    publisher: "Winbean winlite"
    published: "2023-11-21"
  - name: "Frozen Dominion: Galactic Conquest - Kryos"
    image: img/game/video/youtube-kI7Bxi041tg.webp
    link: "https://www.youtube.com/watch?v=kI7Bxi041tg"
    publisher: "Winbean winlite"
    published: "2023-11-21"
  - name: "Subterranean Skirmish: Galactic Conquest - Mini Caves"
    image: img/game/video/youtube-QAt7oXvyPSI.webp
    link: "https://www.youtube.com/watch?v=QAt7oXvyPSI"
    publisher: "Winbean winlite"
    published: "2023-11-20"
  - name: "Subterranean Skirmish: Galactic Conquest - Mini Caves"
    image: img/game/video/youtube-YOwmvU1r374.webp
    link: "https://www.youtube.com/watch?v=YOwmvU1r374"
    publisher: "Winbean winlite"
    published: "2023-11-20"
  - name: "Battle for the Streets: Galactic Conquest - Mos Eisley"
    image: img/game/video/youtube-JQhHu7k1oSY.webp
    link: "https://www.youtube.com/watch?v=JQhHu7k1oSY"
    publisher: "Winbean winlite"
    published: "2023-11-20"
  - name: "Prime Frontiers: Galactic Conquest - Alaris Prime"
    image: img/game/video/youtube-1bRS15QHz68.webp
    link: "https://www.youtube.com/watch?v=1bRS15QHz68"
    publisher: "Winbean winlite"
    published: "2023-11-20"
  - name: "Wandering in the Abyss: Galactic Conquest - Mini Wasteland"
    image: img/game/video/youtube-INVu7mn6U_Q.webp
    link: "https://www.youtube.com/watch?v=INVu7mn6U_Q"
    publisher: "Winbean winlite"
    published: "2023-11-20"
---
