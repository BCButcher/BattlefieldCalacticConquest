---
title: Gameplay Gallery
categories:
  - Battlefield 1942
type: gallery
layout: layouts/gallery.njk
sections:
  - name: Maps
    items:
      - name: Tatooine
        link: https://web.archive.org/web/20231227232422/https://www.bf-games.net/downloads/2936/galactic-conquest-mappack-2011.html
        publisher: Django and HC83, TheoNeo
        published: "2005-01-27, 2011-04-01, 2024-08-17"
        image:
          - img/game/gameplay/tatooine/5440-1293981555.jpg
          - img/game/gameplay/tatooine/2679.jpg
          - img/game/gameplay/tatooine/2682.jpg
          - img/game/gameplay/tatooine/5440-1293981461.jpg
          - img/game/gameplay/tatooine/2683.jpg
          - img/game/gameplay/tatooine/2680.jpg
          - img/game/gameplay/tatooine/2681.jpg
          - img/game/gameplay/tatooine/5440-1293981501.jpg
          - img/game/gameplay/tatooine/19.jpg
          - img/game/gameplay/tatooine/20.jpg
          - img/game/gameplay/tatooine/21.jpg
          - img/game/gameplay/tatooine/23.jpg
          - img/game/gameplay/tatooine/26.jpg
          - img/game/gameplay/tatooine/28.jpg
          - img/game/gameplay/tatooine/31.jpg
          - img/game/gameplay/tatooine/bunker-1.png
          - img/game/gameplay/tatooine/bunker-2.png
      - name: Extreme Prejudice
        link: https://web.archive.org/web/20231227232422/https://www.bf-games.net/downloads/2936/galactic-conquest-mappack-2011.html
        publisher: Django and HC83
        published: "2011-04-01"
        image:
          - img/game/gameplay/extreme-prejudice/2663.jpg
          - img/game/gameplay/extreme-prejudice/2661.jpg
          - img/game/gameplay/extreme-prejudice/2662.jpg
          - img/game/gameplay/extreme-prejudice/5440-1295003594.jpg
          - img/game/gameplay/extreme-prejudice/2665.jpg
          - img/game/gameplay/extreme-prejudice/5440-1295003696.jpg
          - img/game/gameplay/extreme-prejudice/2660.jpg
          - img/game/gameplay/extreme-prejudice/5440-1295003669.jpg
          - img/game/gameplay/extreme-prejudice/5440-1295003708.jpg
          - img/game/gameplay/extreme-prejudice/2664.jpg
          - img/game/gameplay/extreme-prejudice/2666.jpg
      - name: Ryloth
        link: https://web.archive.org/web/20231227232422/https://www.bf-games.net/downloads/2936/galactic-conquest-mappack-2011.html
        publisher: Django and HC83
        published: "2011-04-01"
        image:
          - img/game/gameplay/ryloth/2677.jpg
          - img/game/gameplay/ryloth/2676.jpg
          - img/game/gameplay/ryloth/5440-1293744129.jpg
          - img/game/gameplay/ryloth/2673.jpg
          - img/game/gameplay/ryloth/2674.jpg
          - img/game/gameplay/ryloth/5440-1293744149.jpg
          - img/game/gameplay/ryloth/5440-1293744171.jpg
          - img/game/gameplay/ryloth/2675.jpg
          - img/game/gameplay/ryloth/2678.jpg
          - img/game/gameplay/ryloth/5440-1293744074.jpg
      - name: Ryloth
        publisher: Winbean
        published: "2023-11-22, 2024-05-09"
        image:
          - img/game/gameplay/ryloth/Screenshot_2024-05-06_165950.png
          - img/game/gameplay/ryloth/Screenshot_2024-05-06_165747.png
          - img/game/gameplay/ryloth/Screenshot_2024-05-06_165802.png
          - img/game/gameplay/ryloth/Screenshot_2024-05-06_165828.png
          - img/game/gameplay/ryloth/Screenshot_2024-05-06_165833.png
          - img/game/gameplay/ryloth/Screenshot_2024-05-06_165853.png
          - img/game/gameplay/ryloth/Screenshot_2024-05-06_165946.png
          - img/game/gameplay/ryloth/Screenshot_2023-11-22_000221.png
          - img/game/gameplay/ryloth/Screenshot_2023-11-22_000228.png
          - img/game/gameplay/ryloth/Screenshot_2023-11-22_000314.png
          - img/game/gameplay/ryloth/Screenshot_2023-11-22_000408.png
          - img/game/gameplay/ryloth/Screenshot_2023-11-22_000413.png
          - img/game/gameplay/ryloth/Screenshot_2023-11-22_000540.png
          - img/game/gameplay/ryloth/Screenshot_2023-11-22_000651.png
          - img/game/gameplay/ryloth/Screenshot_2023-11-22_000814.png
          - img/game/gameplay/ryloth/Screenshot_2023-11-22_000828.png
      - name: Endor
        link: https://web.archive.org/web/20231227232422/https://www.bf-games.net/downloads/2936/galactic-conquest-mappack-2011.html
        publisher: Django and HC83, Winbean, Relik
        published: "2011-04-01, 2024-05-09, 2024-08-17"
        image:
          - img/game/gameplay/endor/Screenshot_2024-05-06_164758.png
          - img/game/gameplay/endor/Screenshot_2024-05-06_164706.png
          - img/game/gameplay/endor/2659.jpg
          - img/game/gameplay/endor/2658.jpg
          - img/game/gameplay/endor/2657.jpg
          - img/game/gameplay/endor/5440-1293540096.jpg
          - img/game/gameplay/endor/5440-1293540085.jpg
          - img/game/gameplay/endor/5440-1293540111.jpg
          - img/game/gameplay/endor/5440-1293540119.jpg
          - img/game/gameplay/endor/5440-1293540130.jpg
          - img/game/gameplay/endor/5440-1293540152.jpg
          - img/game/gameplay/endor/5440-1293539473.jpg
          - img/game/gameplay/endor/2656.jpg
          - img/game/gameplay/endor/walker.png
          - img/game/gameplay/endor/r2d2.png
          - img/game/gameplay/endor/imperial-shuttle.png
      - name: Alaris Prime
        link: https://web.archive.org/web/20231227232422/https://www.bf-games.net/downloads/2936/galactic-conquest-mappack-2011.html
        publisher: Django and HC83
        published: "2011-04-01"
        image:
          - img/game/gameplay/alaris-prime/2650.jpg
          - img/game/gameplay/alaris-prime/2649.jpg
          - img/game/gameplay/alaris-prime/5440-1297078369.jpg
          - img/game/gameplay/alaris-prime/2651.jpg
          - img/game/gameplay/alaris-prime/2652.jpg
          - img/game/gameplay/alaris-prime/2653.jpg
          - img/game/gameplay/alaris-prime/2654.jpg
          - img/game/gameplay/alaris-prime/2655.jpg
          - img/game/gameplay/alaris-prime/5440-1297078132.jpg
          - img/game/gameplay/alaris-prime/5440-1297078226.jpg
          - img/game/gameplay/alaris-prime/5440-1297078252.jpg
          - img/game/gameplay/alaris-prime/5440-1297078263.jpg
          - img/game/gameplay/alaris-prime/5440-1297078323.jpg
          - img/game/gameplay/alaris-prime/5440-1297078354.jpg
          - img/game/gameplay/alaris-prime/5440-1297078436.jpg
      - name: Alaris Prime
        publisher: Winbean
        published: "2023-11-22"
        image:
          - img/game/gameplay/alaris-prime/Screenshot_2023-11-21_235247.png
          - img/game/gameplay/alaris-prime/Screenshot_2023-11-21_235318.png
          - img/game/gameplay/alaris-prime/Screenshot_2023-11-21_235330.png
          - img/game/gameplay/alaris-prime/Screenshot_2023-11-21_235343.png
          - img/game/gameplay/alaris-prime/Screenshot_2023-11-21_235409.png
          - img/game/gameplay/alaris-prime/Screenshot_2023-11-21_235438.png
          - img/game/gameplay/alaris-prime/Screenshot_2023-11-21_235542.png
          - img/game/gameplay/alaris-prime/Screenshot_2023-11-21_235609.png
          - img/game/gameplay/alaris-prime/Screenshot_2023-11-21_235702.png
          - img/game/gameplay/alaris-prime/Screenshot_2023-11-21_235723.png
          - img/game/gameplay/alaris-prime/Screenshot_2023-11-21_235913.png
          - img/game/gameplay/alaris-prime/Screenshot_2023-11-22_000007.png
      - name: Mos Eisley
        publisher: Winbean
        published: "2023-11-22"
        image:
          - img/game/gameplay/mos-eisley/Screenshot_2023-11-21_221729.png
          - img/game/gameplay/mos-eisley/Screenshot_2023-11-21_221750.png
          - img/game/gameplay/mos-eisley/Screenshot_2023-11-21_221809.png
      - name: Endor Speederbikes
        link: https://web.archive.org/web/20231227232422/https://www.bf-games.net/downloads/2936/galactic-conquest-mappack-2011.html
        publisher: Django and HC83
        published: "2011-04-01"
        image:
          - img/game/gameplay/endor-speederbikes/5440-1294794508.jpg
          - img/game/gameplay/endor-speederbikes/5440-1294794333.jpg
          - img/game/gameplay/endor-speederbikes/5440-1294794347.jpg
          - img/game/gameplay/endor-speederbikes/5440-1294794360.jpg
          - img/game/gameplay/endor-speederbikes/5440-1294794448.jpg
          - img/game/gameplay/endor-speederbikes/5440-1294794463.jpg
          - img/game/gameplay/endor-speederbikes/5440-1294794482.jpg
          - img/game/gameplay/endor-speederbikes/5440-1294794572.jpg
          - img/game/gameplay/endor-speederbikes/5440-1294794585.jpg
      - name: Kessel
        link: https://web.archive.org/web/20231227232422/https://www.bf-games.net/downloads/2936/galactic-conquest-mappack-2011.html
        publisher: Django and HC83
        published: "2011-04-01"
        image:
          - img/game/gameplay/kessel/5440-1296490724.jpg
          - img/game/gameplay/kessel/5440-1296490706.jpg
          - img/game/gameplay/kessel/5440-1296490824.jpg
          - img/game/gameplay/kessel/5440-1296490902.jpg
          - img/game/gameplay/kessel/5440-1296491113.jpg
          - img/game/gameplay/kessel/2667.jpg
          - img/game/gameplay/kessel/2668.jpg
          - img/game/gameplay/kessel/2669.jpg
          - img/game/gameplay/kessel/2670.jpg
          - img/game/gameplay/kessel/2671.jpg
          - img/game/gameplay/kessel/2672.jpg
          - img/game/gameplay/kessel/5440-1296490758.jpg
          - img/game/gameplay/kessel/5440-1296490856.jpg
          - img/game/gameplay/kessel/5440-1296490914.jpg
          - img/game/gameplay/kessel/5440-1296490952.jpg
      - name: Podrace
        link: https://web.archive.org/web/20231227232422/https://www.bf-games.net/downloads/2936/galactic-conquest-mappack-2011.html
        publisher: Django and HC83
        published: "2011-04-01"
        image:
          - img/game/gameplay/podrace/2644.jpg
          - img/game/gameplay/podrace/2646.jpg
          - img/game/gameplay/podrace/2648.jpg
          - img/game/gameplay/podrace/2643.jpg
          - img/game/gameplay/podrace/2645.jpg
          - img/game/gameplay/podrace/2647.jpg
          - img/game/gameplay/podrace/5440-1294114068.jpg
          - img/game/gameplay/podrace/5440-1294114641.jpg
          - img/game/gameplay/podrace/5440-1294114921.jpg
      - name: Hoth
        link: https://web.archive.org/web/20030628015829/http://www.galactic-conquest.net:80/index.php?page=&action=show&id=2184
        publisher: Galactic-Conquest.net, WinBean
        published: "2003-06-28, 2003-08-29, 2005-01-27, 2024-05-05"
        image:
          - img/game/gameplay/hoth/Screenshot_2024-05-05_223151.png
          - img/game/gameplay/hoth/Screenshot_2024-05-05_223217.png
          - img/game/gameplay/hoth/Screenshot_2024-05-05_223225.png
          - img/game/gameplay/hoth/Screenshot_2024-05-05_223529.png
          - img/game/gameplay/hoth/Screenshot_2024-05-05_223544.png
          - img/game/gameplay/hoth/Screenshot_2024-05-05_223851.png
          - img/game/gameplay/hoth/Screenshot_2024-05-05_223910.png
          - img/game/gameplay/hoth/5899.jpg
          - img/game/gameplay/hoth/5947.jpg
          - img/game/gameplay/hoth/5950.jpg
          - img/game/gameplay/hoth/5995.jpg
          - img/game/gameplay/hoth/6121.jpg
          - img/game/gameplay/hoth/6127.jpg
          - img/game/gameplay/hoth/6958.jpg
          - img/game/gameplay/hoth/12955.jpg
          - img/game/gameplay/hoth/12958.jpg
          - img/game/gameplay/hoth/12961.jpg
          - img/game/gameplay/hoth/12964.jpg
          - img/game/gameplay/hoth/12967.jpg
          - img/game/gameplay/hoth/13042.jpg
          - img/game/gameplay/hoth/13045.jpg
          - img/game/gameplay/hoth/13048.jpg
          - img/game/gameplay/hoth/13051.jpg
          - img/game/gameplay/hoth/13054.jpg
          - img/game/gameplay/hoth/13057.jpg
          - img/game/gameplay/hoth/13060.jpg
          - img/game/gameplay/hoth/13063.jpg
          - img/game/gameplay/hoth/13069.jpg
          - img/game/gameplay/hoth/13072.jpg
          - img/game/gameplay/hoth/13078.jpg
          - img/game/gameplay/hoth/13081.jpg
          - img/game/gameplay/hoth/13084.jpg
          - img/game/gameplay/hoth/13090.jpg
          - img/game/gameplay/hoth/49.jpg
      - name: Deathstar
        published: "2004-08-05"
        image:
          - img/game/gameplay/deathstar/trench.jpg
          - img/game/gameplay/deathstar/x-wing.jpg
          - img/game/gameplay/deathstar/x-wing-cockpit-lancer.jpg
          - img/game/gameplay/deathstar/y-wing-cockpit.jpg
      - name: Bespin
        published: "2005-01-27"
        image:
          - img/game/gameplay/bespin/110.jpg
          - img/game/gameplay/bespin/108.jpg
      - name: Bespin Night
        published: "2024-05-06"
        image:
          - img/game/gameplay/bespin-night/Screenshot_2024-05-05_221503.png
          - img/game/gameplay/bespin-night/Screenshot_2024-05-05_221814.png
          - img/game/gameplay/bespin-night/Screenshot_2024-05-05_221913.png
      - name: Tanaab
        published: "2005-01-27"
        image:
          - img/game/gameplay/tanaab/id-and-tie_interceptor.jpg
          - img/game/gameplay/tanaab/roger-that.jpg
      - name: Judicator
        published: "2005-01-27"
        image:
          - img/game/gameplay/judicator/93.jpg
      - name: Kryos
        publisher: Winbean
        published: "2023-11-22"
        image:
          - img/game/gameplay/kryos/Screenshot_2023-11-22_002900.png
          - img/game/gameplay/kryos/Screenshot_2023-11-22_002913.png
          - img/game/gameplay/kryos/Screenshot_2023-11-22_002947.png
          - img/game/gameplay/kryos/Screenshot_2023-11-22_002955.png
          - img/game/gameplay/kryos/Screenshot_2023-11-22_003123.png
          - img/game/gameplay/kryos/Screenshot_2023-11-22_003138.png
          - img/game/gameplay/kryos/Screenshot_2023-11-22_003221.png
          - img/game/gameplay/kryos/Screenshot_2023-11-22_003237.png
          - img/game/gameplay/kryos/Screenshot_2023-11-22_003300.png
          - img/game/gameplay/kryos/Screenshot_2023-11-22_003308.png
          - img/game/gameplay/kryos/Screenshot_2023-11-22_003348.png
          - img/game/gameplay/kryos/Screenshot_2023-11-22_003354.png
      - name: Mini Caves
        publisher: Winbean
        published: "2023-11-22"
        image:
          - img/game/gameplay/mini-caves/Screenshot_2023-11-21_222514.png
          - img/game/gameplay/mini-caves/Screenshot_2023-11-21_222534.png
          - img/game/gameplay/mini-caves/Screenshot_2023-11-21_222539.png
          - img/game/gameplay/mini-caves/Screenshot_2023-11-21_222609.png
          - img/game/gameplay/mini-caves/Screenshot_2023-11-21_222727.png
      - name: Mini Wayfar
        publisher: Winbean
        published: "2023-11-22, 2024-05-09"
        image:
          - img/game/gameplay/mini-wayfar/Screenshot_2024-05-07_124927.png
          - img/game/gameplay/mini-wayfar/Screenshot_2024-05-07_125007.png
          - img/game/gameplay/mini-wayfar/Screenshot_2024-05-07_124600.png
          - img/game/gameplay/mini-wayfar/Screenshot_2024-05-07_124625.png
          - img/game/gameplay/mini-wayfar/Screenshot_2024-05-07_124700.png
          - img/game/gameplay/mini-wayfar/Screenshot_2024-05-07_124755.png
          - img/game/gameplay/mini-wayfar/Screenshot_2024-05-07_124802.png
          - img/game/gameplay/mini-wayfar/Screenshot_2023-11-21_224051.png
          - img/game/gameplay/mini-wayfar/Screenshot_2023-11-21_224109.png
          - img/game/gameplay/mini-wayfar/Screenshot_2023-11-21_224127.png
          - img/game/gameplay/mini-wayfar/Screenshot_2023-11-21_224156.png
          - img/game/gameplay/mini-wayfar/Screenshot_2023-11-21_224216.png
          - img/game/gameplay/mini-wayfar/Screenshot_2023-11-21_224315.png
          - img/game/gameplay/mini-wayfar/Screenshot_2023-11-21_224348.png
          - img/game/gameplay/mini-wayfar/Screenshot_2023-11-21_224412.png
          - img/game/gameplay/mini-wayfar/Screenshot_2023-11-21_224423.png
      - name: Mini Wasteland
        publisher: Winbean
        published: "2023-11-22, 2024-05-09"
        image:
          - img/game/gameplay/mini-wasteland/Screenshot_2024-05-06_171031.png
          - img/game/gameplay/mini-wasteland/Screenshot_2023-11-21_234426.png
          - img/game/gameplay/mini-wasteland/Screenshot_2023-11-21_234433.png
          - img/game/gameplay/mini-wasteland/Screenshot_2023-11-21_234529.png
          - img/game/gameplay/mini-wasteland/Screenshot_2023-11-21_234613.png
          - img/game/gameplay/mini-wasteland/Screenshot_2023-11-21_234633.png
      - name: Mini Dantooine
        publisher: Winbean
        published: "2024-05-09"
        image:
          - img/game/gameplay/mini-dantooine/Screenshot_2024-05-07_130249.png
---
