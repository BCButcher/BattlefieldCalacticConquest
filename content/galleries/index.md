---
title: Galleries
categories:
  - Battlefield 1942
type: media
layout: layouts/mod.njk
sections:
  - name: Gameplay
    icon: sw/boba-fett.svg
    link: "./gameplay"
  - name: Footage
    icon: sw/scout-trooper.svg
    link: "./footage"
  - name: Assets
    icon: sw/c3po.svg
    link: "./assets"
  - name: Weapons
    icon: sw/han-solo-blaster.svg
    link: "./weapons"
---
