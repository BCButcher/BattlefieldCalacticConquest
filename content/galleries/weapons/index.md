---
title: Weapon Gallery
categories:
  - Battlefield 1942
type: gallery
layout: layouts/gallery.njk
sections:
  - name: Handheld
    items:
      - name: E-11 Blaster Rifle
        image: img/game/weapons/handheld/e-11/render.jpg
      - name: A-295 Blaster Rifle
        image: img/game/weapons/handheld/a-295/render.jpg
      - name: T-21
        image: img/game/weapons/handheld/t-21/render.jpg
      - name: Tracker 16 Light Repeating Blaster
        image: img/game/weapons/handheld/tracker-16/render.jpg
      - name: Dresselian Rifle (Alliance)
        image: img/game/weapons/handheld/dresselian-projectile-rifle/render.jpg
      - name: Disruptor
        image: img/game/weapons/handheld/disruptor/main-render.jpg
      - name: Speeder Pistol
        image: img/game/weapons/handheld/speeder-pistol/render.jpg
      - name: DL-44 Heavy Blaster Pistol
        image:
          - img/game/weapons/handheld/dl-44/render1.jpg
          - img/game/weapons/handheld/dl-44/render2.jpg
          - img/game/weapons/handheld/dl-44/alternate.png
      - name: DH-17 Blaster Pistol
        image:
          - img/game/weapons/handheld/dh-17/render.jpg
          - img/game/weapons/handheld/dh-17/alternate.png
  - name: Utilities
    items:
      - name: Flechette Launcher
        image:
          - img/game/weapons/utility/flechette/render2.jpg
          - img/game/weapons/utility/flechette/render1.jpg
      - name: Gas Grenade
        image: img/game/weapons/utility/gas-grenade/render.jpg
      - name: Detonation Kit
        image: img/game/weapons/utility/detonation-kit/render.jpg
      - name: Mines
        image: img/game/weapons/utility/landmine/render.jpg
      - name: Electrobinoculars
        image: img/game/weapons/utility/electrobinoculars/render.jpg
  - name: Stationary
    items:
      - name: Anti-Vehicle Turret
        image: img/game/stationary/anti-vehicle-turret/render.jpg
---
