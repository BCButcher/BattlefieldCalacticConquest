---
title: Assets Gallery
categories:
  - Battlefield 1942
type: gallery
layout: layouts/gallery.njk
sections:
  - name: Wallpapers
    items:
      - image:
          - img/game/concept-art/wallpapers/2003-06-29_Mike-Landers.jpg
          - img/game/concept-art/wallpapers/2003-06-29_Silencer.jpg
          - img/game/concept-art/wallpapers/2003-06-29_The-Preacher.jpg
          - img/game/concept-art/wallpapers/2003-06-29_XCept1.jpg
  - name: Magazine Articles
    items:
      - name: "The Whys of Modding"
        image:
          - img/game/assets/magazine-articles/gc_computergames1.png
          - img/game/assets/magazine-articles/gc_computergames2.png
          - img/game/assets/magazine-articles/gc_computergames3.png
        publisher: "Brett Todd - Computer Games"
        published: "2004-06"
      - name: "Galactic Conquest v0.1D"
        image: img/game/assets/magazine-articles/gcpcz2.png
        publisher: "Tony Lamb - PCZone"
        published: "2004-01-08"
      - name: "The Mod Crackdown"
        image:
          - img/game/assets/magazine-articles/PCG_May_05_pg22.png
          - img/game/assets/magazine-articles/PCG_May_05_pg23.png
        publisher: "PC Gamer"
        published: "2005-06-05"
  - name: Trailers
    items:
      - name: "Galactic Conquest 0.1a trailer"
        image: img/game/video/tPDUBU-GKzs.jpg
        link: "https://www.youtube.com/watch?v=tPDUBU-GKzs"
        publisher: "Gamer Jedi"
        published: "2006-08-16"
      - name: "Galactic Conquest 0.2 Trailer"
        image: img/game/video/I41BdB6xZio.jpg
        link: "https://www.youtube.com/watch?v=I41BdB6xZio"
        publisher: "Gamer Jedi"
        published: "2006-08-16"
      - name: "Galactic Conquest Release 4 Trailer"
        image: img/game/video/WnuZzRvsK34.jpg
        link: "https://www.youtube.com/watch?v=WnuZzRvsK34"
        publisher: "Gamer Jedi"
        published: "2006-08-16"
      - name: "Galactic Conquest RC 4 Trailer 1"
        image: img/game/video/XKXT3b3g088.jpg
        link: "https://www.youtube.com/watch?v=XKXT3b3g088"
        publisher: "Haakona"
        published: "2013-04-28"
      - name: "Galactic Conquest Release 5 trailer"
        image: img/game/video/vKoTwqMM8OE.jpg
        link: "https://www.youtube.com/watch?v=vKoTwqMM8OE"
        publisher: "Gamer Jedi"
        published: "2006-08-16"
---
