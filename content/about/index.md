---
title: About
categories:
  - Battlefield 1942
layout: layouts/default.njk
origin: https://www.moddb.com/mods/battlefield-galactic-conquest
---

## Game Modes

Battlefield Galactic Conquest supports the Conquest, Capture The Flag, Team Death Match, and Objectives game modes.

<a href="./guide" role="button" secondary>Game Guide</a> <a href="./variants" role="button" secondary>Mod Variants</a>

## History

### Release 0.1

Released at [2003-08-01](https://www.gamefront.com/games/battlefield-1942/file/galactic-conquest-1), it included the map Hoth, and the classes Sniper, Light Infantry, Heavy Infantry, Anti-Armor, and Support. It already had a [variety of weaponry and vehicles](https://www.boards.ie/discussion/107381/mod-news-galactic-conquest):

#### Factions

- Rebel Alliance
- Imperial Forces

#### Rebel Alliance Weapons

- DL-44 Blaster Pistol
- DH-17 Blaster Pistol
- Kylan 3
- A295 Blaster Rifle
- Dressilian Sniper Rifle
- Tracker 16 Repeating Blaster
- Golan Arms Flechette Launcher
- E-web Deployable repeating blaster

#### Rebel Alliance Vehicles

- Incom T47 Air Speeder - Snowspeeder
- YT-1300 Corellian Freighter - Millenium Falcon
- Anti-Vehicle Turret
- Anti-Personnel Turret

#### Imperial Weapons

- Sorosub QS25 Blaster Pistol
- Epel Jaken Razor Series
- E-11 Sniper rifle
- E-11 Blaster Rifle
- TL-21 Light repeating blaster
- Golan Arms Flechette launcher

#### Imperial Vehicles

- Seiner Fleet Systems TIE bomber
- All terrain Scout Transport (AT-ST)

#### Common Weapons

- E-web Deployable repeating blaster
- Knife
- Landmine
- Medipack
- Hydrospanner

#### Other

- Original Weapon, Radio, Vehicle and player sounds
- Complete new background and ingame menu
- Complete new HUD to fit the Star Wars theme

#### Patch 0.1b-e

Subsequent patches fixed many issues around the map and gameplay, including technical issues.

### Release 2

Released at [2004-03-10](https://www.gamefront.com/games/battlefield-1942/file/galactic-conquest-alpha-0-2), it added the maps Beggars Canyon, Bespin, Deathstar, Judicator, Tanaab, Taskforce, and Tatooine. It also introduced new vehicles and ships, stationary units, and weapons.

#### Land

- AT-ST - One new position, new cockpit models
- AT-ST - Desert version
- Echo Trolly
- Landspeeder - Improved skin and code.
- Landspeeder - Modified with an added e-web in passenger position
- Mouse Droid
- R2D2 Droid
- R5D4 Rebel droid
- R5D4 Imperial droid
- Speederbike
- Tramcar
- Uplift

#### Air and Space

- Nebulon-B Frigate
- Carrack Cruiser
- Awing
- Cloud Bus
- Cloudcar
- EscapePod - Rebel blockade Runner version
- EscapePod - Imperal Lancer frigate version
- Lambda Shuttle
- Lancer Frigate
- Millenium Falcon - Improved code and health
- Probe Droid - Improved code
- Rebel Blockade Runner
- Sail Barge
- Skiff
- Slave 1
- Snowspeeder - Improved code, added flaps
- T-16 Skyhopper
- Tie Advanced
- Tie Bomber - Improved code
- Tie Fighter - Improved code
- Tie Interceptor
- Y-Wing
- X-Wing

#### Stationary

- Anti-Personnel Turret - Cockpit model added
- Bacta Tank
- Turbo Laser
- Super Turbo Laser

#### Hand Weapons

- A295
- Concussion Grenade
- Cryoban
- DH-17
- DL-44
- Dressellian Rifle
- E-11
- E-11 Sniper
- Exp Pack
- Flechette
- Fragnade
- Kylan
- Landmine
- PLX-2M
- Razor
- Speeder Pistol
- TL-21
- Tracker 16
- Vibro Blade

#### Other

- Binoculars

#### Other

- Many player models added
- A completly new Damage system
- NEW MUSIC
- New loading screens and loading bar
- Custom Skyboxes
- Engine effects added
- Explosion effects added
- New Echo base model and Hoth facelift

### Release 3

Released at [2004-04-19](https://www.boards.ie/discussion/154867/galactic-conquest-patch-0-3), it added the map Mon Calamari. It introduced some new vehicles and ships, and fixed many smaller and bigger issues.

#### Vehicles

- E-Wing Fighter Craft
- Imperial Wave Skimmer
- Rebel Amphibian
- Refitted Rebel Gunship
- R2-D2 Ground Unit
- Imperial Landing Craft
- Mon Cal player model

### Release 4

Released at [2004-09-09](https://www.gamefront.com/games/battlefield-1942/file/galactic-conquest-full-client), it added the maps Dantooine, Lok, and Anchorhead. It introduced some new vehicles, and fixed many smaller and bigger issues.

#### Vehicles and Stationary units

- Spy Tower
- Missile Turret (Empire)
- Dual Trooper Arial Platform (Empire)
- T-3B Assault Tank (Rebel)
- Firehawke (Imperial)
- T-3BA (Rebel) The rebel artillery unit
- Mobile Imperial Artillery (Imperial)
- Gunship (Rebel) - Not really a new craft, this is a less dominating version of the gunship, geared for troop transport

#### Player Classes

- The game now features 5 NEW main character classes, with 3 sub classes on certain maps
- Many new weapon types added, and abilities such as sprinting, and building shields
- IT IS HIGHLY recommended you read the GC Manual regarding new player classes, as there are certain abilities that require you to press special keys

#### Patch 4.2

Released at [2004-09-15](https://www.gamefront.com/games/battlefield-1942/file/galactic-conquest-patch), it fixed many issues, added tweaks and add-ins.

### 2004 Jolt Tournament Mini Map Pack

Released at [2004-10-13](https://www.bf-games.net/downloads/308/jolt-tournament-gc-infantry-mappack.html), it added the 4 mini maps Caves, Chandrila, Dant, Wayfar.

### Release 5

Released at [2005-02-01](https://www.gamefront.com/games/battlefield-1942/file/galactic-conquest-v5-full), it added the maps Vanguard, Kryos, Corellia, and Mos Eisley. A special race map - Bonus Race - and 2 mini maps - Cove and Wayfar - were also added. It introduced some new vehicles and stationary units, and fixed many smaller and bigger issues.

#### Vehicles and Stationary units

- B-Wing
- Sand Crawler - Used as a mobile spawn fortress for the rebels. Very tough, very big
- I-3 Satellites - 3-man space defense turrets. Used to defend capture points on Vanguard
- YT-600 - Repair craft for Taskforce. Can repair capital ships very quickly
- AT-AT - A mobile spawn, has 6 crew positions. Pilot, Copilot, door operation, and three passengers
- GAIN-135 - 3 seat landspeeder, with a tail gun added for extra fun!
- Sentry Droid - Imperial scout droid that can call in artillery
- Tie Bomber V Variant - On Vanguard, the Tie Bomber comes with rockets rather than bombs

#### Patch 5.3

Released at [2005-05-17](https://www.gamefront.com/games/battlefield-1942/file/galactic-conquest-5-3-client-patch), it fixed many issues, added tweaks and add-ins.

### 2011 Map Pack

Released at [2011-04-01](https://www.bf-games.net/downloads/2936/galactic-conquest-mappack-2011.html), it added the maps Kessel and Extreme Prejudice, and revised the maps Ryloth, Endor, Endor Speederbikes, Podrace, and Alaris Prime. Kessel and Extreme Prejudice was later deprecated. There were also numerous new animated textures and a new menu background.

### Release 8.1

Released at [2015-12-10](https://www.moddb.com/news/galactic-conquest-81-release), it added the maps Vanguard, Kryos, Corellia, and Mos Eisley. A special race map - Bonus Race - and 6 mini maps, were also added. It introduced some new vehicles and stationary units, and fixed many smaller and bigger issues.

#### Patch 8.2

Released at [2017-08-13](https://www.moddb.com/news/galactic-conquest-81-release), it fixed some smaller technical issues.

#### Patch 8.3

This fixed some smaller technical issues.

### Release 8.4

Released at [2021-12-01](https://www.moddb.com/mods/battlefield-galactic-conquest/downloads/galactic-conquest-84), it added the map Bespin Night.

#### Patch 8.5

Released at [2022-11-02](https://www.moddb.com/mods/battlefield-galactic-conquest/downloads/galactic-conquest-85), it fixed some smaller technical issues.

## Team

Many people have contributed to the development of the mod and maps, and the following is a list that combines what was listed in [2004-08-18](https://web.archive.org/web/20040818000124/http://modguide.ngz-network.de/_database/deutsch/datenbank_mods_bf1942_swbattlefield_team.php) and what's currently known. It is probably not exhaustive or entirely accurate for the lifetime of the project's development, but tries to include as many as possible of known contributors.

### Project Leaders

- Szier
- Suge
- JayBiggs
- Menion, early releases
- Wrafe, early releases

### Modellers

- Suge
- JayBiggs
- Luuri
- The_Preacher
- DracoNB
- neko^
- Limper
- Ages120
- Psi^Ka
- Wrafe
- HaVoK
- THEFIEND
- Stukov
- Iceman

### Skinners

- Suge
- THEFIEND
- DracoNB
- Ages120
- Limper
- JayBiggs
- Psi^Ka
- Sensei
- Tempest
- Nudnick
- Wrafe
- DesertSnake

### Mappers

- SZier
- Phantom
- Liam-
- Socketman
- Kiosk
- Henk

### Coders

- AbblePC
- SZier
- Django
- Ages120
- DracoNB
- Psi^Ka
- Redrock
- flrrb

### AI

- Dnamro

### 2D Artists

- The_Preacher
- Xcept1

### Conceptual Artist

- Thomas Stöcker

### Voice Actor

- VocalVoodoo

### Sound Engineers

- Nomen
- Moadn
- Loopfish

### MOD Teaser Video Producer

- White_Rabbit

### Testers

- WeEd
- Module
- Mr.Smiles
- BlasterFreak
- Wardancer
- The|Jesus
- Onworld
- FlippyWarbear
- Juppiteiah
- Straider
- Kampfer
- Loderunner (aka Kurosaji)
- BigBadChewy
- Tulkas
- Brown750
- Renwood
- Sicarius
- Kiffer
- CourtesyFlush
- MUHAHAHAHA
- Herbanator
- KnifeU - Lead tester
- Ed Rimmer
- Grunt
- LTA
- Opti
- Rambo
- Spades
- Plague
- Rams
- buttabean
- DarkSavior
- dt_lexi
- PingoStar
- eddr
- EvilFreelancer
- Phyrex
- Subartica
- JuhorNieger
- The_Preacher
- Weapon-X
- StarKiller
- Triprotic-Acid

### Community Liaison Officer

- KnifeU
- Posi
- TheoNeo

### Website

- Miztah
