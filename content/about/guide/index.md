---
title: Gameplay Guide
categories:
  - Battlefield 1942
type: guide
layout: layouts/default.njk
origin: http://galacticconquest.rf.gd/A1.htm
---

## Capital Ship tips

- They are all controlled with standard drive keys, and can raise elevation higher or lower with the pitch up/pitch down keys - or the keys used to raise and lower ramps.
- They are slow, and once they start turning, they take a long time to stop. It's best to be patient when getting them to turn, and not build up more speed than you can handle.
- They can only be damaged effectively by torpedoes, bombs, or weapons from other capital ships. Ramming, and lasers, only does very minimal damage to these large vehicles.
- Imperial capital ships have a lot more health than Rebel ones, but this is counteracted by the fact the Rebel Fighters carry more torpedoes, and are generally stronger than their Imperial cousins.
- It's best if anyone crewing a capital ship is the Engineer class, so they can repair inbetween battles.
- While you can change positions at any time, full crews make for longer living craft. This is especially important on Task-Force, where each captial ship spawns only once.
- If you can land in a bay - or know a few tricks - you can board capital ships and attempt to storm their bridge. From the bridge, you can kill the bridge crew- and while you can't take control of an enemy ship or stop the players from re-spawning, you can try to buy time for your own capital ships to finish them off. Every ship has at least one way inside.
- Be warned, capital ships will not steer outside the top grid of a space map.
- Most captial ships spawn several waves of craft. Even when one escape pod is taken, another will spawn a short time later, even while the first is till alive. This isn't true for certain vehicles though- for example, the carrack only carried two tie bombers, and won't spawn another wave until the first is destroyed.

## Imperial TIE tips

- On maps like Task force, it is very important imperial Fighters and Interceptors escort and defend the less manuverable bombers. The bombers can cause havoc on enemy fleets, but only if they can get to them.
- Rebel X-Wings and Y-Wings both have torpedos that can cause major damage to Imperial captial ships, as such, TIE fighters should always focus on stoping them first.
- The TIE Advanced is only flyable in the DEATHSTAR map. In Tanaab, it is an objective, and still in construction.
- Don't fire and warn an enemy you on his tail until you have a clear and good shot.
- On DEATHSTAR, when leaving the TIE hangers, be sure to thrust forward slightly, then use full vertical life (with reverse thrust) to glide smoothly out the hanger, and then pitch back, and goto full throttle. If you just gun the TIE full forward off of the rack, it'll go nose first into the ground. (Note, you can see this demoed in the action Release4 trailer).

## Imperial Transport Ship tips

- Your first job in piloting the Lambda or Lander should be to get on the ground, to drop of any troops you have. Remember, there are no Parachoots in GC, so landing is required, even if under fire.
- If taking heavy damage, but near a flag, your almost always better to make a forced landing and get your crew out, rather than try to manuver and loose everyone in the air.
- It's very important the lander keeps constant gunners in it's stations. It can hold off E-Wings pretty well, if maintained.
- While the Lambda and Lander offer pilot guns, you'll be hard pressed to really hit anything with them. Consider them last resort, and usually second to just trying to ram the smaller fighters around you.
- The DTAP does well at picking off infantry, and droping various class types quickly into the field, but can't actually damage heavy tanks. A pilot is best at helping people get around, and shooting at what he can damage. An Engineer in a DTAP can fly ahead, setting up traps or laying mines. A Scout in a DTAP can quickly call in arty strikes.
- While the Imperial landing craft can land in water, remember not to park to far away from where the troops have to go. They can't fight while swimming. Having a few guys cover them from the side door may be worth while.

## Rebel Fighters tips

- The rear gun on the snow speeder can be very effective, even when the speeder is stoped. Setting it up to defend a flag you land at may be worth your while.
- It's very helpful on Deathstar to have people man the droids as you make the run down the trech. It's even more important for the Y-Wing rear gunner to be filled, to protect it from the faster ties coming up behind it.
- Proton Torps are one of only a few weapons the rebels have against capital ships, or exsaust ports. Use them sparingly against defences such as turbo lasers, and only clear out the ones you need to.
- All craft can rearm if they get close enough to their home carriers. There is no re-arm location on Deathstar though. One way trip.
- When taking off on Deathstar, alliance craft are located very far above the surface, and outside the game engines controllable flight range. This is to minimise Imperial forces attacking the rebel launch point. (Not cannon, but nessisary for gameplay). When you launch in a rebel fighter, you'll want to use vertical lift only, to control a safe decent to the Deathstar, and normal flying altitude.
- When taking off from the Nebulon-B Frigate, use a combination of hover and slight forward thrust to break free of the tractor beam and take off. Just using forward thrust will run you into the hanger floor.

## Rebel Transport Ship tips

- Try to land the gunship with as little lateral movement as possible, or it'll tip over.
- Avoid any kind of anti-air defenses with the gunship, at least until you drop troops off. Use the gunships maneuverability to stay in low areas.
- The Falcon on Tanaab has a spawn point inside, and can be used to help deploy troops quickly into the Lab objective.
- Practice practice practice with the Gunship. It can be hard to get the hang of. It takes a subtle touch, and an understanding of how much force to counter the momentum.
- The Gunship ball turrets are only effective against infantry. Targeting at the feet of the infantry is best (rather than directly at) that way the splash damage from the blast can take a larger effect.
- When leaving Echo in the Falcon, stay low with it. You have a roof over your head.

## Rebel Ground Vehicle tips

- Remember that tanks have to be attacked with special weapons, such as rockets, mines, explosion packs, or heavy blasters from other tanks.

## Droid tips

- Droids are great for holding flags. Sometimes if you don't move, people will think you are part of the scenery.
- Repair beams from droids actually do damage at the tip where the beam strikes the target. When the beam is activated though, you heal within a radius around it. So don't directly strike what you are trying to heal, just get close to it.
