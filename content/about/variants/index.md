---
title: Variants
categories:
  - Battlefield 1942
layout: layouts/default.njk
---

In addition to the main mod, there was also made addons, additions, and custom maps that did not ship with it.

## Galactic Conquest Extended

Galactic Conquest Extended (GCX) is a mini-mod that adds bot support to Galactic Conquest and extends the GCmod by doing total conversions of existing maps. It includes these unique maps, that did not come from the main game or main mod: Endoria, Endoth, Frozen Planet, Circarpous IV Raid, Plains Of Sakiya, Planet Bazaria, The Swamp, and Two Moons.

<a href="./extended" role="button" secondary>Galactic Conquest Extended Maps</a>

Further there is Galactic Conquest Redux, which adds singleplayer bot support and tweaks to main original maps, and includes some unique maps as well: Balmorra, Balmorra Race, Jaksonia, Mustafar, Swamps Of Dagobah, and Yavin IV.

<a href="./redux" role="button" secondary>Galactic Conquest Redux Maps</a>

Lastly there are some known custom maps, though possibly quite a lot more. These include: Dogfighting, Dunes, Flood Plains, Imperial Air Training, Imperial Assault, Last Stand, Ralltiir, Redemption, Return To Hoth, and Selonia.

<a href="./custom" role="button" secondary>Custom Maps</a>
