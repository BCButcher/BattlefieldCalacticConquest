---
title: Galactic Conquest Custom Maps
description: >-
  Galactic Conquest Extended (GCX) is a mini-mod that adds bot support to
  Galactic Conquest and extends the GCmod by doing total conversions of existing
  maps.type: source
layout: layouts/source.njk
origin: >-
  https://en.ds-servers.com/gf/battlefield-1942/maps-levels-missions/galactic-conquest/
---
