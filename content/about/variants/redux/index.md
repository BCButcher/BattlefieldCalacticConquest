---
title: Galactic Conquest Redux Maps
description: >-
  Galactic Conquest Redux is a standalone mod that adds bot support to Galactic
  Conquest, and further improves Galactic Conquest Extended.
type: source
layout: layouts/source.njk
origin: >-
  https://web.archive.org/web/20240109172434/https://www.bfmods.com/viewtopic.php?t=2598
---
