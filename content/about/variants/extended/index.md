---
title: Galactic Conquest Extended Maps
description: >-
  Galactic Conquest Extended (GCX) is a mini-mod that adds bot support to
  Galactic Conquest and extends the GCmod by doing total conversions of existing
  maps.
type: source
layout: layouts/source.njk
origin: >-
  https://web.archive.org/web/20051018184752/http://www.planetbattlefield.com/battlefieldsingleplayer/galacticconquest.asp
---
