---
title: Ships
categories:
  - Battlefield 1942
layout: layouts/two-column.njk
grid: "halves thirds"
origin:
  - http://galacticconquest.rf.gd/
  - https://web.archive.org/web/20050128223425/http://www.galactic-conquest.net/index.php?page=&action=show&id=2848
sections:
  - name: Capital Ships
    columns:
      - name: Imperial
        icon: sw/galactic-empire.svg
        items:
          - name: Imperial Carrack Cruiser
            image: img/game/vehicles/carrack.png
            points:
              - Hangar has 4 Tie-Fighters, and 2 Bombers or 2 Interceptors
            description: "The largest ship in GC also sports the largest single location you can get fighters from. 6 fighters on spawn, and replenishes several waves of Tie-Fighters, up to 12 craft in one battlefield at a single time. Has more armor and health than any other capital ship, and two forward mounted heavy Turbolasers. Best view too, that front window spans several decks."
          - name: Imperial Lancer Frigate
            image: img/game/vehicles/lancer.png
            points:
              - Equipped with an escape pod
            description: "The Lancer was the Empires attempt to answer the rebel fighter threat. Armed with several anti-fighter turrets, and more heavily armored than the Blockade Runner. Equal maneuverability with the Blockade runner, although slightly larger. It only has one lower mounted heavy turbo laser, so captains facing other capital ships should get as high as they can."
      - name: Alliance
        icon: sw/rebel-alliance.svg
        items:
          - name: Rebel Nebulon-B Frigate
            image: img/game/vehicles/nebulonb.png
            points:
              - Hangar has 1 X-Wing, and limited A-Wings or Y-Wings
            description: "Not as heavily armored as the carrack, but slightly faster. Has several anti-fighter turrents, and two forward mounted heavy turbo lasers for attacking other capital ships. The arch of the forward turbo lasers is limited in both height and width, and a good Capian should know where his crew can shoot."
          - name: Rebel Blockade Runner
            image: img/game/vehicles/runner.png
            points:
              - Equipped with 2 escape pods
            description: "One of the faster and smaller capital ships, the Blockade runner works very well as an anti-fighter platform. Pilot controlled forward mounted heavy turbo laser, as well as a lower mounted one for another gunner. Has as much firepower as a Nebulon or Carrack, if in the right position. It has much weaker shields and armor, and must rely on it's speed to stay out of the gun zones of Imperial ships."
  - name: Fighters
    columns:
      - name: Imperial
        icon: sw/galactic-empire.svg
        items:
          - name: TIE Fighter
            image: img/game/vehicles/tie-fighter/alternate.jpg
            description: "The classic TIE is the basic air and space defender for the Empire. It has the weakest health of all of the fighter craft in GC, but is quick and capable in a dogfight. It is armed with two forward mounted blasters."
            addendum:
              - Laser cannons
              - 6.3 meters long
              - Starfighter by Sienar Fleet Systems
          - name: TIE Interceptor
            image: img/game/vehicles/tieinterceptor.png
            description: "A very capable space superiority fighter. Armed with four blasters, and one custom proton torpedo launcher for use against heavy targets like tanks or Capital ships. Still has weaker health than most Rebel craft."
          - name: TIE Bomber
            image: img/game/vehicles/tie-bomber/alternate.jpg
            description: "Slow, and not very maneuverable. Carries a load of bombs for use against ground, or capital ship targets. Armed with two forward blasters. Very capable against heavy armor, but needs protection from fighters."
            addendum:
              - Laser cannons, Proton bombs
              - 7.8 meters long
              - Starfighter by Sienar Fleet Systems
          - name: TIE Advanced (X2)
            image: img/game/vehicles/tieadvanced.png
            description: "The experimental TIE that Darth Vader himself flew a version of. The only GC TIE to match the health of rebel fighters, and is unmatched in speed and maneuverability."
      - name: Alliance
        icon: sw/rebel-alliance.svg
        items:
          - name: A-Wing
            image: img/game/vehicles/awing.png
            description: "The fastest of the non-experimental craft, the A-Wing's small size and speed makes it a very hard target to hit. Not surprisingly, this makes it an excellent interceptor. A-Wing's typically have charge of chasing down Tie Bombers, and halting Interceptors. It has two forward lasers, and a center mounted concussion missile launcher for anti-fighter use."
            addendum:
              - Laser cannons, Concussion missiles
              - 9.6 meters long
              - Starfighter / interceptor by Dodonna-Blissex
          - name: X-Wing
            image: img/game/vehicles/x-wing/alternate.jpg
            description: "The X-Wing is the most flexible of all fighters in GC. A good interceptor, armed with 4 lasers, and twin proton torpedo launchers (ammo for 6 torps), the X-Wing can do damage to just about anything. There is a slot for a second crewman to be the R2 droid, and continually heal the X-Wing during flight, giving it lasting power. S-Foils auto open at attack speed."
            addendum:
              - Laser cannons, Proton torpedoes
              - 12.5 meters long
              - Starfighter by Incom Corporation
          - name: Y-Wing
            image: img/game/vehicles/ywing.png
            description: "This bomber carries a much larger load of proton torpedoes than the X-Wing and can shoot four at a time, but is much slower and less maneuverable. To compensate, it has slightly more armor, and two more crew spots; A defensive gunner and R2 droid (for healing)."
          - name: E-Wing
            image: img/game/vehicles/ewing.png
            description: "The newest generation of Rebel craft, the E-Wing is an exclusive to the Battle of Mon Calamari. Faster and better armored than the X-Wing, it functions as a very capable support craft for Atmospheric use."
          - name: B-Wing
            image: img/game/vehicles/bwing.png
          - name: T-16 Skyhopper
            image: img/game/vehicles/skyhopper.png
            description: "The T-16, a native of Tatooine maps, isn't really a fighter at all. However it was adopted from it's recreational use to help serve the rebellion. It as lightly armored as a TIE Fighter, and only armed with a single blaster. However, it can also carry an extra passenger, and functions decently as a transport and light interceptor."
          - name: Snow Speeder
            image: img/game/vehicles/snow-speeder/alternate.jpg
            description: "Used at the Battle of Hoth, this craft is better designed for atmospheric flight than the TIE fighters it battles. Armed with two forward blasters, and with room for a rear gunner, it does a decent job keeping the skies clear over the trench. It has slightly more armor than a tie fighter, but is well short of most other alliance craft."
            addendum:
              - Laser cannons, Harpoon
              - 5.3 meters long
              - Assault Speeder by Incom Corporation
  - name: Transports
    columns:
      - name: Imperial
        icon: sw/galactic-empire.svg
        items:
          - name: Lambda Shuttle
            image: img/game/vehicles/lambda-shuttle/alternate.jpg
            description: "The lambda is the main transport for the empire, and frequently serves as a mobile spawn point in GC. Players will auto crew when spawning in an available Lambda shuttle, which can hold up to 6. The wings, gear, and doors, will automatically open and close upon slowing down and landing. It's armed with 4 forward blasters, and a rear defense turret."
            addendum:
              - Five Double Laser Cannons
              - 20 meters long
              - Multi-purpose shuttle by Sienar Fleet Systems
          - name: Imperial Landing Craft
            image: img/game/vehicles/lander.png
            description: "This behemoth is nearly three times the size of the lambda. It always acts as a mobile spawn point, and is capable of water landings if necessary. The wings, gear, and doors, will automatically open and close upon slowing down and landing. It's armed with two crewable defense turrets on the top of the craft, to provide cover from fighters."
          - name: Dual Trooper Arial Platform
            image: img/game/vehicles/dtap.png
            description: "The DTAP is an evolution of the STAP class transport. It's lightly armed with a forward anti infantry gun for the pilot, and a more powerful medium blaster for the rear gunner. It's primarily used for quickly transporting troops across the battlefield."
      - name: Alliance
        icon: sw/rebel-alliance.svg
        items:
          - name: Millennium Falcon
            image: img/game/vehicles/millennium-falcon/alternate.jpg
            description: "With a good crew, the Falcon comes out as more of a fighter than a transport, a bridge between capital class ships, and the fighters. The falcon pilot can fire concussion missiles, and has crew positions for two very powerful top and bottom turrets. Gear and entry hatch is automatic with speed."
            addendum:
              - Quad turbolaser, Concussion missiles
              - 26.7 meters long
              - YT-1300 freighter by Corellian Engineering Corporation
          - name: Rebel Gunship
            image: img/game/vehicles/gunship.png
            description: "A throwback to a war fought only a few decades before, the rebels salvaged a few of the former Republic Gunships for transport use. They'll take whatever they can get. There are two versions, a better constructed craft with 2 bubble turrets for anti infantry, one nose gunner, and full complement of rockets for the pilot, and version that was less well salvaged. It's slow, but very good at transporting troops and providing ground support against vehicles and infantry."
  - name: Other
    columns:
      - name: Hutt Clan
        icon: sw/hutt-clan.svg
        items:
          - name: Jabba's Palace
            image: img/game/vehicles/sailbarge.png
            description: "The Sail barge has a large and powerful deck gun, and two side rail guns for picking off troops."
          - name: Skiff
            image: img/game/vehicles/skiff.png
            description: "Skiffs are found on Tatooine, and server as air transports from base to base. If you are careful not to fall out while moving, you can use your weapons over the rails at other skiffs, or players on the ground. Skiffs come in handy when looking for alternate ways to take Jabba's, or attack the Sarlaac capture point."
          - name: Slave 1
            image: img/game/vehicles/slave1.png
            description: "This notorious craft isn't exactly nimble in an atmosphere, and is probably one of the hardest GC craft to fly (or at least land). The pilot has rockets and lasers at his command, and a second position operates a bomb drop."
      - name: Neutral Factions
        items:
          - name: Cloud Car
            image: img/game/vehicles/cloudcar.png
            description: "The Cloud Car can be stolen for your cause on Bespin, and is used to attack transports or ferry the pilot across platforms. A second crew position is available, that controls the second forward gun."
          - name: Sand Crawler
            image: img/game/vehicles/sandcrawler.png
          - name: YT-600
            image: img/game/vehicles/yt600.png
            description: "A repair ship for fleets engaging in space."
      - name: Non-combatant
        items:
          - name: Cloud Bus
            image: img/game/vehicles/cloudbus.png
            description: "A transport vehicle on Bespin, can safely transport up to six players, with room to cram more in if needed. Also is useful as a barricade."
          - name: Escape Pods
            image:
              - img/game/vehicles/escpod.png
              - img/game/vehicles/impescpod.png
            points:
              - Limited fuel
              - No armor, very little health
            description: "For those moments when you don't want to go down with the ship. Escape pods fly with normal flight controls, and have a LIMITED amount of fuel. They can be used as transports for assault missions, or to escape a doomed craft. The come in a Rebel and Imperial variety, as seen here."
---
