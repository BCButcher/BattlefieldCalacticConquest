---
title: Battlefield Galactic Conquest
layout: layouts/home.njk
sections:
  - name: Maps
    icon: iconpark/compass-one.svg
    link: "./maps"
  - name: Classes
    icon: sw/stormtrooper2.svg
    link: "./classes"
  - name: Weapons
    icon: sw/han-solo-blaster.svg
    link: "./weapons"
  - name: Ships
    icon: sw/millenium-falcon.svg
    link: "./ships"
  - name: Vehicles
    icon: sw/noun/noun-at-at-2202279-modified.svg
    link: "./vehicles"
  - name: Units
    icon: sw/r2d2.svg
    link: "./units"
  - name: Galleries
    icon: iconpark/new-picture.svg
    link: "./galleries"
  - name: Audio
    icon: iconpark/sound-one.svg
    link: "./audio"
  - name: About
    icon: iconpark/book-one.svg
    link: "./about"
communities:
  - title: Galactic Conquest Discord
    url: "https://discord.gg/KtM223KZkF"
    image: "/img/gc.jpg"
    description: "Plays the mod actively, and regularly hosts events."
    alt: "The Galactic Conquest community plays the mod actively, and regularly hosts events"
  - title: HelloClan
    url: "https://helloclan.eu/bf42-gc82/"
    image: "/img/community/helloclan.jpg"
    description: "Partner who provides server hosting."
    alt: "HelloClan provides server hosting for the Galactic Conquest community"
downloads:
  - items:
      - title: Latest version on HelloClan
        url: "https://forum.helloclan.eu/resources/76/"
        image: "/img/downloads/gc_banner_small.png"
        description: "Full base mod on HelloClan.eu"
        alt: "You can download the full base mod on HelloClan"
      - title: Latest version on ModDB
        url: "https://www.moddb.com/mods/battlefield-galactic-conquest/downloads"
        image: "/img/downloads/moddb gc-banner-small.png"
        description: "Full base mod on ModDB.com"
        alt: "You can download the full base mod on ModDB.com"
---

Galactic Conquest is a full conversion Star Wars themed modification for Battlefield 1942, which mirrors the universe with high precision.

In it you will travel to iconic destinations of the Galactic Civil War and the Clone Wars!

There are no Jedi Powers in the mod, and it was conceived and originally designed in the early era of modding with full permission from LucasArts.
