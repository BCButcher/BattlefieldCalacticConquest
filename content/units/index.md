---
title: Units
categories:
  - Battlefield 1942
layout: layouts/details.njk
grid: "halves thirds"
origin:
  - http://galacticconquest.rf.gd/
  - https://web.archive.org/web/20050128223425/http://www.galactic-conquest.net/index.php?page=&action=show&id=2848
details:
  - name: Droids
    items:
      - name: R2
        image: img/game/vehicles/r2.png
        description: "The R2 unit is a little skittish at the moment. He's capable of repairing and healing, but he's currently only found on the Water world of Mon Calamari, and you'll have to climb in to push him around. R2 will be upgraded, and better distributed in the future. But who can control these guys anyway?"
      - name: R5
        image:
          - img/game/vehicles/r5d4.png
          - img/game/vehicles/r5d4imperial.png
        description: "The most common droid in GC, R5 is primarily a vehicle repair droid. His primary fire heals vehicles, as well as players. He has decent speed to travel, thought is best used in repairing near by craft, or supporting assaults. Comes in an Imperial, and Rebel version."
      - name: Probe
        image: img/game/vehicles/probe-droid/main.png
        description: "The only offensive droid, the Probe droid packs a light laser that can do decent damage to enemy troops. It is also capable of calling in scouting positions, as needed. It's ability to fly makes it far more of a threat in sneaking past enemy lines to take flags."
      - name: Mouse
        image: img/game/vehicles/mousedroid.png
        description: "His day to day tasks of cleaning floors put on hold by the assault, the Mouse Droid can charge the ankles of enemy players. He can also sit on, and take flags. Which allows him to slow down the enemy advance."
  - name: Stationary
    items:
      - name: Imperial Missile Defense System (IMDS)
        image: img/game/stationary/imds.png
        description: "Designed to take out medium to large threats, such as gunships and capital ships. Fires a salvo of six missiles before it has to reload. The travel time of the missile makes it challenging to hit faster fighter craft with, but works very well against larger transports."
      - name: Turbolaser
        image: img/game/stationary/turbolaser.png
        description: "The standard turbo laser is a very capable anti-fighter platform. It's laser rounds can damage almost any armor, to some degree or another, making it very decent at defending against most ground vehicles as well. (Save the heaviest tanks)."
      - name: Large Turbo Laser
        image: img/game/stationary/turbolaser.png
        description: "Found only on the DeathStar (and occasionally at larger fortresses) this Turbo Laser is designed to stop Capital ships in their tracks. Shoots slowly, but a very powerful round that nothing can resist. Not as good at taking on fast moving fighters, however.  The fact that it could house a small army inside it makes it a pretty easy shot for enemy craft."
      - name: Anti-Vehicle Turret
        image: img/game/stationary/anti-vehicle-turret/main.png
        description: "The Anti-Vehicular turret exists to defend the Echo trench from Imperial walkers. Though not an even match for a walker by itself, the trench strings out a long line of them, in coordinated patterns for defense."
      - name: Anti-Personell Turret
        image: img/game/stationary/apturret.png
        description: "The Anti-Personnel turrets on Hoth are designed to stop Imperial ground troops from penetrating to close to the AV and outer defenses."
  - name: Utility
    items:
      - name: R5 Droid Control
        image:
          - img/game/stationary/r5d4control.png
          - img/game/stationary/r5d4controlimperial.png
      - name: Probe Droid Control
        image: img/game/stationary/probedroidcontrol.png
      - name: Mouse Droid Control
        image: img/game/stationary/mousedroid-control.png
      - name: Droid Capsule
        image: img/game/stationary/droidcapsule.png
      - name: Spy Turret
        image: img/game/stationary/spyturret.png
      - name: Bacta Tank
        image: img/game/stationary/bactatank.png
      - name: Lift
        image: img/game/stationary/uplift.png
        description: "When a Stormtrooper needs to get someplace higher, the Uplift is there. This elevator travels up and down. Believe it or not, a good Uplift pilot can make or break an assault. Stopping on the wrong floor is never a good thing. Nor is leaving half the team behind in your rush. Can be crewed by 6, and has standing room for 12"
---
