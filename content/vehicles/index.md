---
title: Vehicles
categories:
  - Battlefield 1942
layout: layouts/two-column.njk
grid: "halves thirds"
origin:
  - http://galacticconquest.rf.gd/
  - https://web.archive.org/web/20050128223425/http://www.galactic-conquest.net/index.php?page=&action=show&id=2848
sections:
  - name: Ground
    columns:
      - name: Imperial
        icon: sw/galactic-empire.svg
        items:
          - name: All-Terrain Armored-Transport (AT-AT)
            image: img/game/vehicles/at-at/alternate.jpg
            addendum:
              - Chin Guns and Side Guns
              - 15.5 meters tall
              - Walker by Kuat Drive Yards
          - name: All-Terrain Scout-Transport (AT-ST)
            image: img/game/vehicles/at-st/main.png
            description: "A dependable ground attack craft. While lacking the punch of a heavy cannon, it's two forward mounted blasters are more than a match for infantry and most medium armored vehicles. Also comes equipped with a pilot used grenade launcher, or rocket rack. Room for two extra crew, a side gunner and commander. The commander has the ability to call in scouting locations."
            addendum:
              - Laser cannons, Concussion grenade launcher
              - 8.6 meters tall
              - Scout walker by Kuat Drive Yards
          - name: Firehawke
            image: img/game/vehicles/firehawke.png
            description: "As good as it gets for the empire on the ground, the Firehawke is fast, and equipped with a  heavy blast cannon. While not as heavy armored as it's rebel counter-part, the T-3B, it's speed more than makes up for it."
          - name: Speeder Bike
            image: img/game/vehicles/speederbike.png
            description: "The fastest way to get around on the ground for the Empire. Speederbikes can carry two players, and are armed with a single forward mounted blaster."
          - name: Mobile Imperial Artillery Unit (MIAU)
            image: img/game/vehicles/impart.png
            description: "Much faster in motion than the Rebel T-3BA, the imperial Artillery fires single shot non-self-propelled round, with a medium arch. It's splash damage is quite large, as such, the minimum distance it can shoot is quite far, making it very vulnerable to targets that get in under it's aim."
          - name: Wave Skimmer
            image: img/game/vehicles/waveskimmer.png
            description: "This watercraft has positions for a forward mounted gunner, and a rear anti-infantry gun to protect it's boarding ramp. Medium armor means it's vulnerable to most blaster fire."
          - name: Tram Car
            image:
              - img/game/vehicles/tramcar/main.png
              - img/game/vehicles/tramcar/alternate.jpg
            description: "A light car for quick transport of a group of up to 6 troops. Open air, but hey, it beats walking."
      - name: Alliance
        icon: sw/rebel-alliance.svg
        items:
          - name: Landspeeder
            image: img/game/vehicles/landspeeder.png
            description: "Next to the speederbike or swoop, the landspeeder is the fastest ground transport in the game, and critical to the Rebel strategy of deployment on Tatooine. It can carry two players, and offers more protection that the bikes do, although as with all hover craft, it can take a bit to get used to it's handling."
          - name: T-3B
            image: img/game/vehicles/rebtank.png
            description: "The T-3B is the rebels top vestment in taking the Empire head to head in the field. Lacking in speed, the T-3B makes up for it in good armor, two heavy blasters, and more health than the Imperial Firehawke. A second crew position for short range artillery is also available, and best used to soften an area on approach."
          - name: T-3Ba
            image: img/game/vehicles/rebart.png
            description: "Built on the T-3B chassy, it has all the same qualities, but packs a much stronger and longer range attack. Fires a round equivalent to that of the MIAU, but does so in a 3 burst volley, causing massive devastation, once zeroed in on a target. However, it suffers from slow speed in deployment, and longer reload times than the Imperial Artillery."
          - name: Amphibion
            image: img/game/vehicles/amph.png
            description: "A Mon Calamari craft, it is capable of travel over both land and water! Primarily used as a transport, it also has a second gunner position for the remote rear mounted turret, which can function to attack enemy ships, or air targets."
          - name: Echo Trolley
            image: img/game/vehicles/echotrolley.png
            description: "If you're on Hoth, and you need to get somewhere, the Echo trolley has your ticket. It has room for several passengers, and also has a build in medical and ammo station, allowing soldiers to use it as a rally point."
---
