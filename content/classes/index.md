---
title: Classes
categories:
  - Battlefield 1942
type: mod
layout: layouts/details.njk
origin: https://web.archive.org/web/20030829080205/http://www.galactic-conquest.net/index.php?page=&action=show&id=2187
bodyclass: classes
details:
  - name: Core Kits
    items:
      - name: Trooper
        image:
          - img/game/kits/assault-axis.png
          - img/game/kits/assault-allies.png
        points:
          - name: E-11 Blaster Rifle (Empire)
            image: img/game/weapons/handheld/icons/e11.png
            link: ../weapons/#e-11-blaster-rifle
          - name: A295 (Alliance)
            image: img/game/weapons/handheld/icons/a295.png
            link: ../weapons/#a-295-blaster-rifle
          - name: Speeder Pistol
            image: img/game/weapons/handheld/icons/speederpistol.png
            link: ../weapons/#speeder-pistol
          - name: Grenade
            image: img/game/weapons/utility/icons/fragnade.png
            link: ../weapons/#merr-sonn-c-22-frag-grenade
          - name: Vibroblade
            image: img/game/weapons/handheld/icons/alliesknife.png
            link: ../weapons/#vibroblade
        description: "Troopers form the backbone of both the Rebel and Imperial forces. They are the most balanced combat class, equipped with weapons that combine a strong rate of fire, accuracy, and range. While not having the longest range, or the best rate of fire, the E-11 or A295 the Troopers use is the best balance between."
        addendum:
          - Lasers and Blasters in Star Wars have a longer travel time than bullets in real life. When playing online, you'll want to remember to lead your targets.
          - Your accuracy is always much better when not running, and improved when crouching or laying down. The Trooper is very flexible, in that he can do a decent job of running and gunning against point blank targets, and if he is laying down or crouching, can be a decent sniper.
          - Trooper grenades will instantly kill other infantry, and can add damage to vehicles, especially lightly armored ones. Three grenades is not near enough to take out larger tanks, but every little bit helps.
          - The more you arch your aim back, the further you'll throw grenades.
          - There is no reload time on either the E11 or A295, but watch your total charge meter, and reload where necessary.
          - A trooper backed by a medic is especially deadly.
      - name: Scout
        image:
          - img/game/kits/scout-axis.png
          - img/game/kits/scout-allies.png
        points:
          - name: E-11 Blaster Rifle Special (Empire)
            image: img/game/weapons/handheld/icons/e11s.png
            link: ../weapons/#e-11-sniper-rifle
          - name: Dresselian Rifle (Alliance)
            image: img/game/weapons/handheld/icons/dresselian.png
            link: ../weapons/#dressellian-projectile-rifle
          - name: Camouflage
            image: img/game/weapons/utility/icons/cammo.png
            link: ../weapons/#camouflage
          - name: Vibroblade
            image: img/game/weapons/handheld/icons/alliesknife.png
            link: ../weapons/#vibroblade
        description: "Scouts are by far the most mobile class in GC, and are masters us using the terrain around them. Equipped with long range single-shot rifles, the scouts can kill any troop on the battlefield in two shots, with a well placed shot taking up to 95% of a players health. Scouts also have the unique ability to sprint, which allows them to cover ground much faster, and escape danger. Scouts can also deploy cammofauge, to better hide them in the battlefield, a useful trick when calling in scout locations for artillery, though not perfect against someone looking hard."
        addendum:
          - 'Has the ability to "sprint". A meter over your health icon shows how much stamina you have. Note: Sprinting into walls, or over rough terrain can damage or kill the player.'
          - 'Has the "camo" ability to lay down a cover. This cover will be destroyed quickly if the scout leaves a close range. You can see out, others can''t see in. It''s designed to blend somewhat with the polys and colors of maps, but you''ll have to be skilled in placement to take full advantage of this skill.'
          - Can call in targets for artillery.
          - Imperial snipers have a recharge time between rounds, but never have to reload until they are out of ammo.
          - Rebel snipers use a projectile based gun, which has a faster round, but needs to be reloaded with clips.
      - name: Medic
        image:
          - img/game/kits/medic-axis.png
          - img/game/kits/medic-allies.png
        points:
          - name: Disruptor (Empire)
            image: img/game/weapons/handheld/icons/distuptor2.png
            link: ../weapons/#disruptor
          - name: Disruptor (Alliance)
            image: img/game/weapons/handheld/icons/distuptor1.png
            link: ../weapons/#disruptor
          - name: Lethal Injection
            image: img/game/weapons/utility/icons/lethal-injection.png
            link: ../weapons/#lethal-injection
          - name: Gas Grenade
            image:
              - img/game/weapons/utility/icons/impgasgren.png
              - img/game/weapons/utility/icons/rebgasgren.png
            link: ../weapons/#gas-grenade
          - name: Medical Pack
            image: img/game/weapons/utility/icons/medpack.png
            link: ../weapons/#medical-pack
        description: "Medics are the strongest support class for infantry combat. Their heal rate is phenomenal, meaning that other classes backed by a healing medic will be much more effective at winning infantry battles. Medics also have several unique abilities and weapons, which help to round out their combat role on the battlefield. "
        addendum:
          - Equipped with toxic Gas Grenades, which only damage the opposing team, cause slow damage over time while players are inside their area of effect. Very good for closed spaces, or blocking of certain areas. Also can provide movement cover.
          - Equipped with "Lethal Injection", a one-shot-kill melee weapon. The range for it is very close, so you have to be right up on the target. The reload time after a shot is quite long, making the medic vulnerable. In a pinch, or if sneaking from behind, it's very effective.
      - name: Anti Vehicular
        image:
          - img/game/kits/antitank-axis.png
          - img/game/kits/antitank-allies.png
        points:
          - name: Flechette Launcher (Empire)
            image: img/game/weapons/utility/icons/flechette.png
            link: ../weapons/#flechette-launcher
          - name: Missile Launcher (Alliance)
            image: img/game/weapons/utility/icons/plx2m.png
            link: ../weapons/#merr-sonn-plx-2m-missile-system
          - name: Speeder Pistol
            image: img/game/weapons/handheld/icons/speederpistol.png
            link: ../weapons/#speeder-pistol
          - name: Mines
            image:
              - img/game/weapons/utility/icons/impmine.png
              - img/game/weapons/utility/icons/rebmine.png
              - img/game/weapons/utility/icons/landmine.png
            link: ../weapons/#mines
          - name: Vibroblade
            image: img/game/weapons/handheld/icons/alliesknife.png
            link: ../weapons/#vibroblade
        description: "The AV class is the only player class specifically tooled to offensively fight enemy ground vehicles. AV troops are equipped with rocket launchers, capable of dealing damage to almost all vehicles, save the largest capital ships. AV rockets are particularly effective against tanks, light vehicles, and aircraft that have not taken off yet. The slow travel time of the rockets makes it difficult to hit aircraft already in the air."
        addendum:
          - Equipped with rocket launchers, which shoot up to 4 rounds per reload, and are effective against ground vehicles and turrets.
          - Equipped with mines, which will instantly kill any tank that rolls over it, and do heavy damage to walkers, and medium damage to lighter flying vehicles that may fly over while taking off.
      - name: Engineer
        image:
          - img/game/kits/engineer-axis.png
          - img/game/kits/engineer-allies.png
        points:
          - name: Detonation Kit
            image: img/game/weapons/utility/icons/demokit.png
            link: ../weapons/#detonation-kit
          - name: Shield
            image: img/game/weapons/utility/icons/shield.png
            link: ../weapons/#shield
          - name: Speeder Pistol
            image: img/game/weapons/handheld/icons/speederpistol.png
            link: ../weapons/#speeder-pistol
          - name: Hydrospanner
            image: img/game/weapons/utility/icons/hydrospanner.png
            link: ../weapons/#hydrospanner
          - name: Vibroblade
            image: img/game/weapons/handheld/icons/alliesknife.png
            link: ../weapons/#vibroblade
        description: "Engineers are very much a supporting class, but very critical to a successful assault or defense. Among other things, the engineer is the only class that can repair vehicles, a key element to winning - especially on maps that feature large multi-crewed craft. Engineers are also critical on some objective maps, being the only class with Detpacks, which can damage any type of armor."
        addendum:
          - Engineers carry explosive detonation kits, which can be placed, and then detonated with a remote. they are very effective against all vehicle types, infantry, and required to destroy objectives on certain maps. Detonation kits can be picked up with the Hydrospanner tool.
          - Engineers can construct defensive shields once per life. These shield have power to last for several minutes, if the engineer remains close enough to keep an eye on it. Shields completely block all energy based weapons from passing into or out of its field. Players can run through a shield at minimal damage cost, as can vehicles. Shields do not block grenade type weapons from passing through, but do protect from the splash damage if they are on the other side when the detonate.
          - Equipped with mines, which will instantly kill any tank that rolls over it, and do heavy damage to walkers, and medium damage to lighter flying vehicles that may fly over while taking off.
  - name: Additional Kits
    items:
      - name: Heavy Weapons
        image:
          - img/game/kits/heavy-imperial.png
          - img/game/kits/heavy-rebel.png
        points:
          - name: T-21 (Empire)
            image: img/game/weapons/handheld/icons/tl21.png
            link: ../weapons/#t-21-light-repeating-blaster
          - name: Tracker 16 (Alliance)
            image: img/game/weapons/handheld/icons/tracker16.png
            link: ../weapons/#tracker-16-light-repeating-blaster
          - name: Vibroblade
            image: img/game/weapons/handheld/icons/alliesknife.png
            link: ../weapons/#vibroblade
        description: The Heavy Weapons class is not one of the five core classes, and is only available in very specific locations. It's an alternative in place of engineer kits on Bespin, and in place of scouts on the space maps. Heavy Weapons kits are also available as pick up items in maps such as Judicator, Hoth, and Dantooine.
        addendum:
          - Heavy weapons is very effective in close infantry combat, having the fastest fire rate and damage of any other gun. However, it's accuracy and power is reduced as targets get further away.
      - name: Light Weapons
        image:
          - img/game/kits/assault-axis.png
          - img/game/kits/assault-allies.png
        points:
          - name: Scout Pistol (Empire)
            image: img/game/weapons/handheld/icons/speederpistol.png
            link: ../weapons/#speeder-pistol
          - name: DL-44 (Alliance)
            image: img/game/weapons/handheld/icons/dl44.png
            link: ../weapons/#dl-44-heavy-blaster-pistol
          - name: Vibroblade
            image: img/game/weapons/handheld/icons/alliesknife.png
            link: ../weapons/#vibroblade
        description: While not technically a class, Light Weapons - or Ironman as its called - is the custom kit used on Judicator, and starts the game only armed with a pistol and vibroblade. On Judicator, every class is Ironman, and in order to get a better gun, players are required to visit armory racks and pick them up. This is a key feature of the map, to add more excitement to the combat. It also means as the battle progresses, one side or the other will gain access to better weapons.
      # - name: Jedi
      #   points:
      #     - name: Lightsaber, one of four colors
      #     - name: Force Lightning
      #     - name: Force Push
      #     - name: Medical Pack
      #       image: img/game/weapons/utility/icons/medpack.png
      #     - name: Hydrospanner
      #       image: img/game/weapons/utility/icons/hydrospanner.png
      #   description: Only found in the ruins on Kessel (H5).
---

## Rebel Alliance

The more the Empire tightened its grip on the people of the galaxy, the harder the Alliance to restore the Republic fought. Born shortly after Palpatine's transformation of the Old Republic into the New Order, the Alliance started as little more than a rag-tag group of freedom fighters woefully under-equipped to challenge an enemy as mighty as the Galactic Empire. The continued injustices of the Empire, however, brought many into the Rebellion's fold.

## Galactic Empire

From the bloated carcass of the Old Republic, an ambitious politician carved the Galactic Empire, a New Order of government meant to sweep away the injustices and inefficiencies of its predecessor.

Rather than offer the people of the galaxy newfound hope, the Empire instead became a tyrannical regime, presided over by a shadowy and detached despot steeped in the dark side of the Force. Personal liberties were crushed, and the governance of everyday affairs was pulled away from the senate, and instead given to unscrupulous regional governors.

Accompanying the growth of the Empire was an unprecedented military buildup. The many shipyards in the Emperor's domain churned out immense fleets of Star Destroyers and TIE fighters. The Imperial starfleet maintained order in the galaxy, a role previously undertaken by the Jedi Knights, an august order of protectors wiped out during the Emperor's ascent.
