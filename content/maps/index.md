---
title: Maps
description: The Battlefield 1942 Galactic Conquest mod features many, varied maps. From intense combat-driven infantry ones to medium-scale ones focusing on armor-combat, as well as large-scale ones with capital ships in space. Many maps combine different combat of different scales.
categories:
  - Battlefield 1942
layout: layouts/maps.njk
---
