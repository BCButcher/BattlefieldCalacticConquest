---
title: Mini Chandrila
author: GC Team
description: >-
  The Peaks of Chandrila were once used for Jedi physical endurance tests before
  the underground Jedi Academy was abandoned. With the increase of rebel
  activity in the sector the Empire has sent scout troopers to investigate the
  academy and surrounding area...
date: '2004-10-13'
categories:
  - Battlefield 1942
  - 2004 Tournament Infantry Mini Mappack
tags:
  - Conquest
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
  size: 1024
  source: 2004-tournament-infantry-mini-mappack
  controlpoints:
    - name: Northern Peak
      id: Northern_Peak
      position:
        x: '532.10'
        'y': '422.17'
        z: '604.89'
    - name: Southern Peak
      id: Southern_Peak
      position:
        x: '523.80'
        'y': '422.15'
        z: '444.12'
    - name: Eastern Peak
      id: Eastern_Peak
      position:
        x: '616.06'
        'y': '407.78'
        z: '528.28'
    - name: Western Peak
      id: Western_Peak
      position:
        x: '450.21'
        'y': '421.56'
        z: '525.77'
  created: '2004-10-13'
  creator: GC Team
---

