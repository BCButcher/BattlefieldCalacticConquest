---
title: Jolt Tournament GC Infantry Mappack
description: >-
  The Official Jolt GC Infantry Tournament is almost upon us! These maps have been designed as 4v4 player, infantry-only, maps.
categories:
  - Battlefield 1942
type: source
layout: layouts/source.njk
origin: >-
  https://web.archive.org/web/20231228001054/https://www.bf-games.net/downloads/308/jolt-tournament-gc-infantry-mappack.html
---
