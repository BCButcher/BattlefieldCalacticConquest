---
title: Mini Wayfar
author: GC Team
description: >-
  Jabba the Hutt has been using Wayfar's black market to sell weapons to rebel
  agents. The Empire has learned of situation and has moved in troops to end all
  illicit activity. Take control of the town and push back enemy advancement to
  secure your victory...
date: '2004-10-13'
categories:
  - Battlefield 1942
  - 2004 Tournament Infantry Mini Mappack
tags:
  - Conquest
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
  size: 1024
  source: 2004-tournament-infantry-mini-mappack
  controlpoints:
    - name: Imperial Base
      id: Imperial_Base
      position:
        x: '522.06'
        'y': '76.13'
        z: '470.06'
    - name: Rebel Base
      id: Rebel_Base
      position:
        x: '525.70'
        'y': '76.11'
        z: '631.78'
    - name: Bazaar
      id: Bazaar
      position:
        x: '485.59'
        'y': '76.19'
        z: '548.31'
    - name: Cargo Hold
      id: Cargo_Hold
      position:
        x: '560.15'
        'y': '76.20'
        z: '550.89'
  created: '2004-10-13'
  creator: GC Team
---

