---
title: Extreme Prejudice
author: Django, GC Team
description: >-
  Carrying vital information about Alliance movements in the sector, the ISD
  Extreme-Prejudice prepares its engines for the jump to hyperspace. The rebels,
  not wanting the Empire to learn of their operations nearby, launch a desperate
  raid against the Star Destroyer. Not having time to destroy it in full, they
  must focus their efforts in taking out the hangar and interior before it can
  compute the jump to lightspeed. But before they can do that they'll have to
  knock out the shield generators protecting it...
date: '2011-04-01'
categories:
  - Battlefield 1942
  - 2011 Mappack
tags:
  - Conquest
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
  size: 4096
  source: 2011-mappack
  controlpoints:
    - name: Isd Hanger
      id: isd_hanger
      position:
        x: '966.31'
        'y': '362.84'
        z: '3630.85'
    - name: Checkpoint Brigde Isd Interior
      id: checkpoint_brigde_isd_interior
      position:
        x: '3472.95'
        'y': '1556.98'
        z: '3814.43'
    - name: Checkpoint 3 Isd Interior
      id: checkpoint3_isd_interior
      position:
        x: '3615.73'
        'y': '1556.78'
        z: '3832.75'
    - name: Checkpoint 1 Isd Interior
      id: checkpoint1_isd_interior
      position:
        x: '3812.22'
        'y': '1556.80'
        z: '3837.42'
    - name: Checkpoint 0 Isd Interior
      id: checkpoint0_isd_interior
      position:
        x: '3979.15'
        'y': '1556.52'
        z: '3815.27'
    - name: Reb Fighter Base
      id: reb_fighter_base
      position:
        x: '2135.26'
        'y': '747.41'
        z: '1687.13'
    - name: Reb Nebulon
      id: reb_nebulon
      position:
        x: '3094.93'
        'y': '537.52'
        z: '1115.68'
  created: '2011-04-01'
  creator: Django, GC Team
origin: https://www.bf-games.net/downloads/2936/galactic-conquest-mappack-2011.html
---

