---
title: Mon Calamari
author: Szier, Luuri, GC Team
description: ''
date: '2004-04-20'
categories:
  - Battlefield 1942
  - Original
tags:
  - Conquest
labels:
  - Release 3
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
  size: 2048
  source: original
  controlpoints:
    - name: Imperial Forward Command
      id: Imperial_Forward_Command
      position:
        x: '374.61'
        'y': '104.69'
        z: '1707.12'
    - name: City Dock
      id: City_Dock
      position:
        x: '1453.79'
        'y': '94.57'
        z: '1421.30'
    - name: City Pump Station
      id: City_Pump_Station
      position:
        x: '1330.31'
        'y': '94.42'
        z: '1289.26'
    - name: Southern Pump Station
      id: Southern_Pump_Station
      position:
        x: '1539.76'
        'y': '93.50'
        z: '475.55'
    - name: Southern Habitat
      id: Southern_Habitat
      position:
        x: '474.00'
        'y': '93.50'
        z: '513.03'
  created: '2004-04-20'
  creator: Szier, Luuri, GC Team
origin: https://www.shacknews.com/article/31537/new-galactic-conquest
---

