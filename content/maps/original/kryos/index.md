---
title: Kryos
author: GC Team
description: >-
  Imperial spies have learned of a top secret weapon being constructed for the
  Rebels some where in the Mon Cal system. A Star Destroyer was sent to
  investigate all outlying planets. When the ship approached Sep Elopon, they
  picked up energy readings coming from the frozen moon Kryos. When the Star
  Destroyer moved into orbit around Kryos, it was nearly destroyed from fire
  from the surface. Unfortunately for the Rebels, the Empire was able to detach
  a ground assault force to the planet in hopes to destroy this new weapon and
  learn where the Rebels' new secret base is...
date: '2005-04-02'
categories:
  - Battlefield 1942
  - Original
tags:
  - Conquest
labels:
  - Release 5
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
  size: 2048
  source: original
  controlpoints:
    - name: Imperial Base
      id: Imperial_Base
      position:
        x: '579.19'
        'y': '80.55'
        z: '434.41'
    - name: Rebel Base
      id: Rebel_Base
      position:
        x: '1513.43'
        'y': '75.33'
        z: '1756.74'
    - name: Ion Cannon
      id: Ion_Cannon
      position:
        x: '1399.55'
        'y': '107.89'
        z: '643.69'
    - name: Water Cooling Station
      id: Water_Cooling_Station
      position:
        x: '1532.51'
        'y': '81.23'
        z: '1096.77'
    - name: Power Generator
      id: Power_Generator
      position:
        x: '860.88'
        'y': '96.42'
        z: '992.02'
  created: '2005-04-02'
  creator: GC Team
origin: >-
  http://www.bf-games.net/downloads/299/galactic-conquest-client-v05-full-installer.html
---

