---
title: Endor
author: GoosemanXP, Django, JayBiggs, GC Team
description: >-
  Endor is a planet covered in jungled forests lightly broken with mountainous
  areas, lakes and rivers. Endor was the location of the Deathstar Mk II's
  shield generator that protected the Deathstar while it was still being
  constructed, knocked out by the Rebel Alliance in order to start the fall of
  the Empire. Endors tall treeline is enough to give any man vertigo and its
  overgrown vegetation leave very little large open areas. Endor is home to the
  ewoks, cute little bear like creatures who use ancient methods of hunting with
  spears and bow and arrow


  **Terrain**: Large dense forests, rough terrain, shallow lakes/rivers and
  mountains.


  **Inhabitants**: Ewoks


  **Occupants**: Imperial forces
date: '2003-03-23'
categories:
  - Battlefield 1942
  - Original
tags:
  - Conquest
labels:
  - Release 2
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
  size: 1024
  source: original
  controlpoints:
    - name: Bunker
      id: Bunker
      position:
        x: '635.88'
        'y': '107.23'
        z: '849.04'
    - name: Rebel Landing
      id: Rebel_Landing
      position:
        x: '439.10'
        'y': '126.64'
        z: '203.70'
    - name: Imperial Outpost
      id: Imperial_Outpost
      position:
        x: '489.91'
        'y': '121.55'
        z: '515.64'
    - name: Ewok Village
      id: Ewok_Village
      position:
        x: '274.97'
        'y': '157.14'
        z: '357.76'
    - name: Landing Platform
      id: Landing_Platform
      position:
        x: '422.76'
        'y': '152.86'
        z: '760.63'
  created: '2003-03-23'
  creator: GoosemanXP, Django, JayBiggs, GC Team
origin: >-
  https://web.archive.org/web/20030829074346/http://www.galactic-conquest.net/index.php?page=&action=show&id=2189
---

