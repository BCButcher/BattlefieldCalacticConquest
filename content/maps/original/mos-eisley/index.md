---
title: Mos Eisley
author: GC Team
description: ''
date: '2005-04-02'
categories:
  - Battlefield 1942
  - Original
tags:
  - Conquest
labels:
  - Release 5
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
  size: 1024
  source: original
  controlpoints:
    - name: Northwest
      id: northwest
      position:
        x: '384'
        'y': '17.2183'
        z: '640'
    - name: Southwest
      id: southwest
      position:
        x: '384'
        'y': '0'
        z: '384'
    - name: Centralcp
      id: centralcp
      position:
        x: '512.942'
        'y': '21.6004'
        z: '510.185'
    - name: Southeast
      id: southeast
      position:
        x: '640'
        'y': '2.19533'
        z: '384'
    - name: Northeast
      id: northeast
      position:
        x: '641.501'
        'y': '13.0479'
        z: '662.526'
    - name: Centralcp 2
      id: centralcp2
      position:
        x: '511.976'
        'y': '55.6691'
        z: '435.789'
  created: '2005-04-02'
  creator: GC Team
origin: >-
  http://www.bf-games.net/downloads/299/galactic-conquest-client-v05-full-installer.html
---

