---
title: Anchorhead
author: GC Team
description: >-
  Imperial intelligence has noticed an increase of rebel ativity in the small
  town of Anchorhead. A legion of Stormtroopers escorting an ATST has raided the
  southern port and prepares to take control of the town. Rebel troops quickly
  gather to revolt against the inevitable battle. Capture and Defend the key
  locations around the town to secure your victory
date: '2004-09-11'
categories:
  - Battlefield 1942
  - Original
tags:
  - Conquest
labels:
  - Release 4
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
  size: 1024
  source: original
  controlpoints:
    - name: Axis Base
      id: AxisBase
      position:
        x: '434.01'
        'y': '71.15'
        z: '699.01'
    - name: Allied Base
      id: AlliedBase
      position:
        x: '732.03'
        'y': '73.68'
        z: '406.66'
    - name: Rebel Starport
      id: Rebel_Starport
      position:
        x: '592.16'
        'y': '65.95'
        z: '332.37'
    - name: Imperial Starport
      id: Imperial_Starport
      position:
        x: '367.82'
        'y': '66.08'
        z: '552.08'
    - name: Cantina
      id: Cantina
      position:
        x: '544.90'
        'y': '81.58'
        z: '514.87'
    - name: Abandoned Outpost
      id: Abandoned_Outpost
      position:
        x: '367.94'
        'y': '93.42'
        z: '331.86'
  created: '2004-09-11'
  creator: GC Team
origin: https://www.boards.ie/discussion/186606/mod-release-galactic-conquest-0-4
---

