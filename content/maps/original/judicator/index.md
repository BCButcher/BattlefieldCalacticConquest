---
title: Judicator
author: Szier, Suge, GC Team
description: ''
date: '2003-03-23'
categories:
  - Battlefield 1942
  - Original
tags:
  - Conquest
labels:
  - Release 2
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
  size: 2048
  source: original
  controlpoints:
    - name: Imperial Bridgetemplate
      id: imperial_bridgetemplate
      position:
        x: '1577.56'
        'y': '569.728'
        z: '875.429'
    - name: Rebelblockadetemplate
      id: rebelblockadetemplate
      position:
        x: '541.448'
        'y': '341.897'
        z: '849.9'
    - name: Lift Controltemplate
      id: lift_controltemplate
      position:
        x: '1642.22'
        'y': '341.4'
        z: '992.665'
    - name: Level 1 Security Roomtemplate
      id: level1_security_roomtemplate
      position:
        x: '557.613'
        'y': '341.4'
        z: '925.225'
    - name: Tram Controltemplate
      id: tram_controltemplate
      position:
        x: '559.195'
        'y': '341.473'
        z: '1032.48'
    - name: Checkpoint 1 Template
      id: checkpoint_1template
      position:
        x: '628.396'
        'y': '341.473'
        z: '925.057'
    - name: Checkpoint 3 Template
      id: checkpoint_3template
      position:
        x: '1049.26'
        'y': '341.473'
        z: '920.298'
    - name: Checkpoint 6 Template
      id: checkpoint_6template
      position:
        x: '1552.56'
        'y': '341.473'
        z: '882.177'
  created: '2003-03-23'
  creator: Szier, Suge, GC Team
origin: https://www.shacknews.com/article/30970/new-galactic-conquest
---

