User-agent: *
Allow: /

Host: https://battlefieldpirates.com/

Sitemap: https://battlefieldpirates.me/sitemap.xml
