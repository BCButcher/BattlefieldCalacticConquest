const shiki = require("shiki");
// const diff = require("./local_modules/shiki-plugin-diff");
const t = shiki.loadTheme("../../local_modules/shiki-themes/nord.json");

const highlight = (code, lang, highlighter) => {
  const tokenized = highlighter.codeToThemedTokens(code, lang);
  const lineOptions = [];
  const theme = highlighter.getTheme();
  return shiki.renderToHtml(tokenized, {
    lineOptions,
    bg: theme.bg,
    fg: theme.fg,
    langId: lang,
  });
};

module.exports = (eleventyConfig, options) => {
  eleventyConfig.amendLibrary("md", () => {});
  eleventyConfig.on("eleventy.before", async () => {
    const highlighter = await shiki.getHighlighter(options);
    // highlighter.usePlugin(diff);
    eleventyConfig.amendLibrary("md", (mdLib) =>
      mdLib.set({
        highlight: (code, lang) => highlighter.codeToHtml(code, { lang }),
      })
    );
  });
};
