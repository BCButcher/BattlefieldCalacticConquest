module.exports = {
  title: "Battlefield Galactic Conquest",
  domain: "BattlefieldGalacticConquest.com",
  url: "https://battlefieldgalacticconquest.com",
  image: "/social.webp",
  language: "en",
  language_bcp47: "en-GB",
  language_icu: "en_GB",
  tags: "battlefield galactic conquest, battlefield 1942",
  description:
    "Galactic Conquest is a full conversion Star Wars themed modification for Battlefield 1942, which mirrors the universe with high precision. In it you will travel to iconic destinations of the Galactic Civil War and the Clone Wars!",
  footer:
    "[BattlefieldGalacticConquest.com](https://gitlab.com/BCButcher/BattlefieldCalacticConquest) by [Butcher](https://gitlab.com/BCButcher). All content copyright by their respective, original authors.",
  author: {
    name: "Butcher",
    url: "https://gitlab.com/BCButcher",
    image: "/logo.png",
  },
  type: "WebPage",
};
