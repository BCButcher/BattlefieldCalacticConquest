const gulp = require("gulp");
const critical = require("critical");

gulp.task("critical", () => {
  return gulp
    .src("./_site/**/*.html")
    .pipe(
      critical.stream({
        inline: true,
        base: "./_site",
        css: ["./_site/css/app.css"],
        target: "./_site",
      })
    )
    .on("error", function (err) {
      console.error(err.message);
    })
    .pipe(gulp.dest("./_site"));
});
