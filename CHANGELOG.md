# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [2.0.5] - 2025-01-12

### Added

- Event video footage from Winbean

### Fixed

- Template-class

## [2.0.4] - 2025-01-01

### Fixed

- Classes-page icon-size and alignment
- Header links color regression
- Upgrade Social image generation dependency

## [2.0.3] - 2024-12-01

### Added

- Event video footage from Omabroodje

## [2.0.2] - 2024-12-01

### Fixed

- Social image generation in `<meta>`

## [2.0.1] - 2024-12-01

### Added

- Event video footage from Winbean

### Fixed

- More distinct link-styling and correct background-position in headers
- More explicit `role="link"`-styling for external links

### Removed

- Superfluous event-page

## [2.0.0] - 2024-09-22

### Changed

- Separated `/galleries/footage/` into subpages, keeping known events with multiple videos at the top level
- HTML-rendering, loading, optimizations to improve performance and compliance
- Image optimization to ~91% size less than original
- Adjusted size of hero-image on home-layout

### Fixed

- "Page not found"-page
- Image links and duplicates
- WAI-ARIA issues
- Styling-issues

### Added

- `exhibit`-template as a simplified display of gallery items, whereas the `gallery`-template is broader
- `local-web-server` for testing and benchmarking closer to production

## [1.4.4] - 2024-08-18

### Added

- Various images to `/galleries/gameplay/`

### Changed

- Reduced treshold and quality of optimized images

## [1.4.3] - 2024-07-23

### Fixed

- Image optimization

## [1.4.2] - 2024-07-23

### Added

- Event images

### Fixed

- 2023-05-06 event name

## [1.4.1] - 2024-07-23

### Changed

- Reordered `/galleries/footage` by event names

### Fixed

- Social image generation

### Added

- Description and origin-link to catalog-items

## [1.4.0] - 2024-07-23

### Changed

- More robust and simpler Social image generation
- Simplify image optimization

## [1.3.0] - 2024-07-07

### Added

- Audio page and assets
- Enable individual audio-file description

### Changed

- More coherent icons on front page

### Fixed

- Map-description for Vanguard
- Vector-icon fill

## [1.2.10] - 2024-06-26

### Added

- Event video footage from Winbean and SuperFjordmannen

## [1.2.9] - 2024-05-09

### Added

- Event video footage and screenshots from Winbean

## [1.2.8] - 2024-05-09

### Added

- Event video footage and screenshots from Winbean

## [1.2.7] - 2024-05-01

### Fixed

- Rebuild

## [1.2.6] - 2024-05-01

### Fixed

- Social Sharing Image

## [1.2.5] - 2024-04-28

### Fixed

- Release

## [1.2.4] - 2024-04-28

### Changed

- Allow nested structures in Gallery

### Fixed

- Social Sharing Image

## [1.2.3] - 2024-04-13

### Added

- Social-sharing image preview in footer

## [1.2.2] - 2024-04-13

### Fixed

- Twitter Social-sharing (OpenGraph) images

## [1.2.1] - 2024-04-13

### Fixed

- Social-sharing (OpenGraph) images

## [1.2.0] - 2024-04-13

### Added

- Generate and apply social-sharing (OpenGraph) images

## [1.1.5] - 2024-03-28

### Fixed

- Rebuild

## [1.1.4] - 2024-03-28

### Added

- Event video footage from Winbean

## [1.1.3] - 2024-03-28

### Added

- Event video footage from Winbean

## [1.1.2] - 2024-03-27

### Added

- Event video footage from Winbean
- Event video footage from 0poIE

### Fixed

- Heading padding

## [1.1.1] - 2024-01-23

### Added

- Event video footage from LuccaWulf
- Post-processing of images to optimize loading

### Fixed

- Banner-icon size
- Social-sharing image
- License
- Revert using Universally Unique IDentifier for gallery identifiers, use simpler, consistent method instead

## [1.1.0] - 2024-01-20

### Added

- Gameplay images from of Winbean
- Event video footage from Winbean

### Fixed

- Use Universally Unique IDentifier for gallery identifiers to avoid naming-conflicts
- Image-link styles

## [1.0.6] - 2024-01-11

### Fixed

- Staff list, Bespin Night

## [1.0.5] - 2024-01-11

### Fixed

- Footer

## [1.0.4] - 2024-01-11

### Fixed

- Staff list

## [1.0.3] - 2024-01-10

### Fixed

- Footer links
- Metadata

## [1.0.2] - 2024-01-10

### Fixed

- Staff list and map creators

## [1.0.1] - 2024-01-09

### Fixed

- Removed some metadata from non-core maps

## [1.0.0] - 2024-01-09

### New

- First public release
